# Introducción

_<mark style="color:yellow;">`***`</mark><mark style="color:yellow;">**`Este curso está en construcción`**</mark><mark style="color:yellow;">`***`</mark>_\
_<mark style="color:yellow;">**`*** De momento estamos terminando la versión en Español`**</mark><mark style="color:yellow;">`***`</mark>_
------------------------------------------------------------------------------------------------------------------------------------------

<figure><img src=".gitbook/assets/image (46).png" alt=""><figcaption></figcaption></figure>

|                                                                  |                                                           |
| :--------------------------------------------------------------: | :-------------------------------------------------------: |
| [**¿Quieres hacer el curso en Español?**](introduccion/espanol/) | [**Vols fer el curs en Català?**](introduccion/catala.md) |
|               ![](<.gitbook/assets/image (40).png>)              |           ![](<.gitbook/assets/image (26).png>)           |
|                     **ESPAÑOL / CASTELLANO**                     |                    **CATALÀ / CATALÁN**                   |
