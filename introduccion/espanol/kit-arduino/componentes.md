# Componentes

Aquí tienes el listado completo de los componentes que encontrarás en nuestro kit!

<table><thead><tr><th width="254">COMPONENTE:</th><th width="372.3333333333333">IMAGEN:</th><th>CANTIDAD:</th></tr></thead><tbody><tr><td><strong>Arduino Uno:</strong><br><strong>- (genérico)</strong></td><td><img src="../../../.gitbook/assets/ARDUINO UNO (2).jpg" alt="" data-size="original"></td><td>1</td></tr><tr><td><strong>Cable Arduino USB:</strong></td><td><img src="../../../.gitbook/assets/USB wire arduino.jpg" alt=""></td><td>1</td></tr><tr><td><strong>Pulsadores:</strong></td><td><img src="../../../.gitbook/assets/Pulsador.jpg" alt=""></td><td>4</td></tr><tr><td><strong>Potenciometros:</strong></td><td><img src="../../../.gitbook/assets/potenciometro (1).jpg" alt=""></td><td>2</td></tr><tr><td><strong>LEDs:</strong><br>- Rojos<br>- Amarillos<br>- Verdes<br>- Azules.<br>- Blancos</td><td><img src="../../../.gitbook/assets/LEDs.jpg" alt=""></td><td>25</td></tr><tr><td><strong>Resistencias:</strong><br>- 10kΩ<br>- 220Ω<br></td><td><img src="../../../.gitbook/assets/10K ohms.jpg" alt=""></td><td>40</td></tr><tr><td>Protoboard</td><td><img src="../../../.gitbook/assets/Protoboard.jpg" alt=""></td><td>1</td></tr><tr><td><strong>Cables de conexión:</strong><br>- Macho / Macho<br>- Macho / Hembra<br>- Hembra / Macho</td><td><img src="../../../.gitbook/assets/jumperwires.png" alt=""></td><td>120</td></tr></tbody></table>



* Placa tipo Arduino UNO.
* Protoboard de 400 pines.
* Jumper Wires MM / HM / HH.
* Juego de Resistencias.
* LEDs de varios colores.
* Botones tipo pulsador.
* Sensor de luz LDR.
*
