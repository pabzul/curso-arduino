# 5 - Resistencias: Colores, Ley de Ohms y Cálculo

¡Hola nuevamente, creadores de POWAR STEAM! En este artículo, continuaremos explorando los conceptos básicos de electricidad que hemos estado aprendiendo en nuestro video tutorial. En esta segunda parte, nos enfocaremos en dos temas cruciales: colores de las resistencias, la Ley de Ohm y cálculo de resistencias. Estos conceptos nos permitirán comprender cómo funciona la corriente eléctrica en un circuito y cómo podemos calcular y utilizar las resistencias adecuadas.



## **Identificación de Resistencias y sus Valores**

Las resistencias son componentes electrónicos clave en cualquier circuito. Son pequeños dispositivos que limitan el flujo de corriente en un circuito y nos permiten controlar la cantidad de corriente que fluye. Para trabajar con resistencias correctamente, es importante saber cómo identificarlas y comprender sus valores.

<figure><img src="../../../.gitbook/assets/image (9).png" alt="resistencias eléctricas, resistors"><figcaption></figcaption></figure>

En las resistencias, generalmente encontramos bandas de colores que representan su valor. Cada color tiene un valor numérico asignado y una tolerancia. El valor de una resistencia se expresa en ohmios (Ω) y se determina por la combinación de colores en las bandas de la resistencia. La tolerancia indica el rango de variación permitido para el valor nominal de la resistencia.

<figure><img src="../../../.gitbook/assets/image (31).png" alt=""><figcaption></figcaption></figure>

Aprender a leer y comprender estos códigos de colores es esencial para identificar y utilizar resistencias de manera adecuada en un circuito. Existen herramientas en línea y aplicaciones móviles que facilitan la interpretación de las bandas de colores y proporcionan información precisa sobre el valor de resistencia.

##

## **La Ley de Ohm**

La Ley de Ohm establece una relación fundamental en los circuitos eléctricos. Esta ley nos dice que la corriente que fluye a través de un conductor es directamente proporcional al voltaje aplicado e inversamente proporcional a la resistencia del circuito. Matemáticamente, podemos expresar esto como V = I x R, donde V representa el voltaje, I representa la corriente y R representa la resistencia. En otras palabras, si aumentamos el voltaje o disminuimos la resistencia, la corriente también aumentará.

<figure><img src="../../../.gitbook/assets/image (13).png" alt="ley de ohm"><figcaption><p><a href="https://cdn.todamateria.com/imagenes/ley-de-ohm1-0-cke-edited-1.jpg">https://cdn.todamateria.com/imagenes/ley-de-ohm1-0-cke-edited-1.jpg</a></p></figcaption></figure>

El cálculo de resistencias en un circuito nos permite determinar el valor de una resistencia necesaria para controlar la corriente eléctrica de acuerdo con nuestras necesidades. Para calcular la resistencia en un circuito, utilizamos la Ley de Ohm. Si conocemos el voltaje y la corriente que deseamos, podemos utilizar la fórmula R = V / I para obtener el valor de resistencia necesario. Esto nos permite seleccionar la resistencia adecuada para proteger nuestros componentes y garantizar un funcionamiento seguro y eficiente del circuito.

Recuerda que las resistencias vienen en diferentes valores estándar. Al seleccionar una resistencia, es importante elegir el valor más cercano al resultado del cálculo, ya que no siempre encontraremos una resistencia con un valor exacto. Podemos combinar resistencias en serie o en paralelo para obtener valores específicos y ajustar la resistencia total en un circuito.

Con estos conceptos, ahora estás más preparado para comprender cómo se comporta la corriente eléctrica en un circuito y cómo utilizar las resistencias adecuadas para controlarla. En futuros tutoriales, exploraremos más a fondo estos temas y los aplicaremos en emocionantes proyectos de POWAR STEAM.
