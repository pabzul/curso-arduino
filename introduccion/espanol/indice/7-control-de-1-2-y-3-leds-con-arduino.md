---
description: Ilumina tu Creatividad con Arduino
---

# 7- Control de 1, 2 y 3 LEDs con Arduino

¡Bienvenidos a otro emocionante capítulo de nuestros tutoriales de electrónica con Arduino! En el fascinante mundo de Arduino, los LEDs son componentes esenciales para dar vida a tus proyectos electrónicos. En este artículo, te guiaremos a través de tres emocionantes tutoriales que te enseñarán cómo controlar LEDs utilizando Arduino. Comenzaremos con un LED básico, luego avanzaremos hacia dos LEDs que se encienden y apagan alternándose como una sirena, y finalmente, crearemos un semáforo utilizando tres LEDs. ¡Prepárate para iluminar tu camino hacia la diversión y el aprendizaje!

<figure><img src="../../../.gitbook/assets/image (32).png" alt=""><figcaption></figcaption></figure>

##

## **¿Qué es un LED?**

Un LED (Light Emitting Diode) es un dispositivo que emite luz cuando se le aplica corriente eléctrica. Tiene dos terminales, el ánodo y el cátodo. La polaridad del LED es importante, ya que debe conectarse correctamente para que funcione correctamente.

<figure><img src="../../../.gitbook/assets/image (20).png" alt=""><figcaption></figcaption></figure>

Para identificar la polaridad de un LED, puedes observar su forma física. El ánodo es generalmente más largo que el cátodo y puede tener una pestaña plana en la parte superior. También puede haber una marca en el cuerpo del LED que indica el cátodo. Si no hay una marca visible, puedes usar un multímetro para medir la resistencia y determinar cuál terminal tiene mayor resistencia, lo que corresponderá al cátodo.

Es esencial conectar el ánodo del LED al terminal positivo de la fuente de alimentación y el cátodo al terminal negativo. Si inviertes la polaridad, el LED no funcionará correctamente y no emitirá luz. Además, aplicar una corriente excesiva o invertida puede dañar permanentemente el LED.

##

## **Lista de componentes necesarios:**&#x20;

Antes de comenzar, asegurémonos de tener los siguientes componentes a mano para nuestros proyectos:

<table><thead><tr><th width="254">COMPONENTE:</th><th width="372.3333333333333">IMAGEN:</th><th align="center">CANTIDAD:</th></tr></thead><tbody><tr><td>Arduino Uno</td><td><img src="../../../.gitbook/assets/ARDUINO UNO (2).jpg" alt="" data-size="original"></td><td align="center">1</td></tr><tr><td>Cable Arduino USB</td><td><img src="../../../.gitbook/assets/USB wire arduino (1).jpg" alt=""></td><td align="center">1</td></tr><tr><td>LED (rojo, amarillo y verde)</td><td><img src="../../../.gitbook/assets/LEDs.jpg" alt=""></td><td align="center">3</td></tr><tr><td>Resistencia de 220Ω</td><td><img src="../../../.gitbook/assets/220 ohms.png" alt=""></td><td align="center">3</td></tr><tr><td>Protoboard</td><td><img src="../../../.gitbook/assets/Protoboard.jpg" alt=""></td><td align="center">1</td></tr><tr><td>Cables de conexión</td><td><img src="../../../.gitbook/assets/jumperwires.png" alt=""></td><td align="center">1</td></tr></tbody></table>

## **Proyecto 1: Encendido y Apagado de un LED**

En esta primera sección, vamos a aprender cómo conectar un LED a Arduino utilizando una resistencia. Esto es fundamental para proteger el LED de corrientes excesivas y asegurarnos de que funcione correctamente. Aquí tienes los pasos a seguir:



### **1 Conexiónes:**

1. Conecta el ánodo del LED (el terminal más largo) al pin digital 13 de Arduino.
2. Conecta el cátodo del LED (el terminal más corto) a la resistencia de 220 ohmios.
3. Conecta el otro extremo de la resistencia al GND (tierra) de Arduino.

Recuerda que la resistencia se coloca en serie con el LED para limitar la corriente que fluye a través de él. De esta manera, evitamos dañar el LED y prolongamos su vida útil.

<figure><img src="../../../.gitbook/assets/1 - Led Blink2.png" alt=""><figcaption></figcaption></figure>

Ahora que tenemos nuestro LED conectado correctamente, vamos a aprender cómo encenderlo y apagarlo utilizando Arduino.&#x20;

###

### **2 Código:**

<figure><img src="../../../.gitbook/assets/image (30).png" alt=""><figcaption></figcaption></figure>

1. Abre el software Arduino IDE en tu computadora.
2. Crea un nuevo proyecto y asigna el pin digital 13 como salida.
3. En el bucle principal (`void loop()`) del programa, utiliza la función `digitalWrite()` para establecer el pin 13 en HIGH (encendido).
4. Espera un breve tiempo utilizando la función `delay()`.
5. Utiliza la función `digitalWrite()` nuevamente para establecer el pin 13 en LOW (apagado).
6. Espera otro breve tiempo antes de repetir el ciclo.

```cpp
int ledPin = 13;                 // Pin del LED

void setup() {
  pinMode(ledPin, OUTPUT);       // Configura el pin del LED como salida
}

void loop() {
  digitalWrite(ledPin, HIGH);   // Enciende el LED
  delay(1000);                  // Espera 1 segundo
  
  digitalWrite(ledPin, LOW);    // Apaga el LED
  delay(1000);                  // Espera 1 segundo antes de repetir el ciclo
}
```



Puedes modificar el valor pasado a la función `delay()` para ajustar el tiempo de espera y crear diferentes efectos de encendido y apagado del LED. ¡Experimenta y diviértete!

Con este sencillo programa, el LED se encenderá y apagará continuamente. ¡Prueba diferentes tiempos de espera para crear diferentes efectos!



## Proyecto 2: Sirena con 2 LEDs alternándose

En este proyecto, vamos a utilizar dos LEDs y controlar su encendido y apagado para simular una sirena. El LED conectado al pin 13 se encenderá y apagará en secuencia con el LED conectado al pin 12. Aquí tienes los pasos:



### **1 Conexiones:**

1. Deja el primer LED (rojo) del proyecto anterior conectado al pin 13.
2. Conecta el ánodo del segundo LED (amarillo) al pin digital 12 de Arduino.
3. Conecta el cátodo del segundo LED (amarillo) a otra resistencia de 220 ohmios.
4. Conecta el otro extremo de la resistencia al GND (tierra) de Arduino.

<figure><img src="../../../.gitbook/assets/2 - 2 Led Blink_bb.png" alt=""><figcaption></figcaption></figure>



### 2 Código:

1. Abre el software Arduino IDE en tu computadora. Crea un nuevo proyecto y asigna los pines digitales 13 y 12 como salidas.&#x20;
2. En el bucle principal (void loop()) del programa, utiliza la función digitalWrite() para encender y apagar los LEDs en secuencia.&#x20;
3. Espera un breve tiempo utilizando la función delay() entre cada cambio de estado.

```cpp
int ledPin = 13;       // Pin del LED Amarillo
int ledPin = 12;       // Pin del LED Verde

void setup() {
  pinMode(13, OUTPUT); // Configura el pin 13 como salida
  pinMode(12, OUTPUT); // Configura el pin 12 como salida
}

void loop() {
  digitalWrite(13, HIGH); // Enciende el LED1 en el pin 13
  digitalWrite(12, LOW); // Apaga el LED2 en el pin 12
  delay(500); // Espera 0.5 segundos
  
  digitalWrite(13, LOW); // Apaga el LED1 en el pin 13
  digitalWrite(12, HIGH); // Enciende el LED2 en el pin 12
  delay(500); // Espera 0.5 segundos antes de repetir el ciclo
}
```



Con este programa, los LEDs conectados a los pines 13 y 12 se encenderán y apagarán alternándose en secuencia, creando un efecto similar a una sirena.

Al igual que en el proyecto anterior, puedes modificar los tiempos de delay entre los LEDs para realizar diferentes efectos.



## Proyecto 3: Construcción de un semáforo con tres LEDs

En este proyecto, vamos a utilizar tres LEDs para construir un semáforo. Cada LED representará un estado del semáforo: rojo, amarillo y verde. Aquí tienes los pasos:



<figure><img src="../../../.gitbook/assets/3 - Semáforo_bb.png" alt=""><figcaption></figcaption></figure>

### 1 - Conexiónes:

1. Conecta el ánodo del primer LED (LED1, rojo) al pin digital 13 de Arduino.
2. Conecta el cátodo del primer LED (LED1, rojo) a la resistencia de 220 ohmios.
3. Conecta el otro extremo de la resistencia al GND (tierra) de Arduino.
4. Conecta el ánodo del segundo LED (LED2, amarillo) al pin digital 12 de Arduino.
5. Conecta el cátodo del segundo LED (LED2, amarillo) a otra resistencia de 220 ohmios.
6. Conecta el otro extremo de la resistencia al GND (tierra) de Arduino.
7. Conecta el ánodo del tercer LED (LED3, verde) al pin digital 11 de Arduino.
8. Conecta el cátodo del tercer LED (LED3, verde) a otra resistencia de 220 ohmios.
9. Conecta el otro extremo de la resistencia al GND (tierra) de Arduino.



### **2 - Código:**

1. Abre el software Arduino IDE en tu computadora.
2. Crea un nuevo proyecto y asigna los pines digitales 13, 12 y 11 como salidas.
3. En el bucle principal (void loop()) del programa, utiliza la función digitalWrite() para controlar los estados del semáforo.
4. Espera un tiempo adecuado utilizando la función delay() entre cada cambio de estado.

```cpp
void setup() {
  pinMode(13, OUTPUT); // Configura el pin 13 como salida para el LED rojo
  pinMode(12, OUTPUT); // Configura el pin 12 como salida para el LED amarillo
  pinMode(11, OUTPUT); // Configura el pin 11 como salida para el LED verde
}

void loop() {
  digitalWrite(13, HIGH); // Enciende el LED rojo
  digitalWrite(12, LOW); // Apaga el LED amarillo
  digitalWrite(11, LOW); // Apaga el LED verde
  delay(3000); // Espera 3 segundos
  
  digitalWrite(13, LOW); // Apaga el LED rojo
  digitalWrite(12, HIGH); // Enciende el LED amarillo
  digitalWrite(11, LOW); // Apaga el LED verde
  delay(1000); // Espera 1 segundo
  
  digitalWrite(13, LOW); // Apaga el LED rojo
  digitalWrite(12, LOW); // Apaga el LED amarillo
  digitalWrite(11, HIGH); // Enciende el LED verde
  delay(3000); // Espera 3 segundos antes de repetir el ciclo
}
```



Con este programa, los LEDs conectados a los pines 13, 12 y 11 simularán el funcionamiento de un semáforo, cambiando de estado en secuencia (rojo, amarillo, verde) y repitiendo el ciclo.

¡Ahí lo tienes! Has aprendido cómo controlar LEDs utilizando Arduino en diferentes configuraciones. Desde un LED individual hasta un semáforo completo, las posibilidades son infinitas. Espero que hayas disfrutado de estos tutoriales y que te sientas inspirado para crear tus propios proyectos con LEDs.

¡Diviértete explorando y experimentando con POWAR STEAM y Arduino!
