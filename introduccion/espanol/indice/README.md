# Indice

1. [**¿Qué es Arduino?**](1-que-es-arduino.md)
2. [**Arduino IDE: Descarga e instalación del software**](2-arduino-ide-descarga-e-instalacion.md)&#x20;
3. [**Programando con Arduino: LED Blink y comunicación serial**](3-aprendiendo-a-programar-con-arduino-led-blink-y-hello-world.md)
4. [**Introducción al Voltaje, Corriente y Resistencia**](4-introduccion-al-voltaje-corriente-y-resistencia.md)
5. [**Resistencias: Identificación de Colores y Ley de Ohm**](5-resistencias-colores-ley-de-ohms-y-calculo.md)
6. [**Conexiones: El Protoboard y sus componentes.**](6-introduccion-a-la-protoboard-y-sus-componentes.md)
7. [**Controlando 1, 2 y 3 LEDs con Arduino**](7-control-de-1-2-y-3-leds-con-arduino.md)
8. [**Señales Analógicas y Digitales: Potenciómetros y Botones Pulsadores**](8-senales-analogas-y-digitales-con-pulsador-y-potenciometro.md)
9. [**Introducción a los Sensores**](9-introduccion-a-los-sensores.md)
10. [**Sensor de Luz (LDR)**](10-sensor-de-luz.md)
11. [**Introducción a los Actuadores**](11-introduccion-a-los-actuadores.md)
12. [**Uso de un motor DC como actuador**](12-motor-dc.md)
13. [**Pantalla LCD y conexión I2C**](pantalla-lcd-i2c.md)
14. **Sensor humedad de plantas y lluvia**
15. [**Sensor de temperatura y humedad DHT11**](13-sensor-de-temperatura-y-humedad-dht11.md)
16. [**Sensor Ultrasónico de Distancia**](14-sensor-ultrasonico-de-distancia.md)
17. **Uso de un servo motor como actuador**
18. **Sensor de sonido y Buzzer**
19. **Control Digital y Análogo: Relé y Mosfett**
20. **Funciones integrando sensores y actuadores**
