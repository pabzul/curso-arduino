# 8 - Señales Análogas y Digitales con pulsador y potenciómetro

<figure><img src="../../../.gitbook/assets/image (57).png" alt=""><figcaption></figcaption></figure>

¡Bienvenidos de nuevo a nuestro canal de tutoriales de Arduino! En este emocionante capítulo, exploraremos los pines digitales, analógicos y PWM, y los utilizaremos en dos emocionantes proyectos utilizando un botón pulsador para encender y apagar un led, y un potenciómetro para controlar su intensidad. Aprenderemos a conectar los componentes, leer y controlar las señales, y crear interacciones interesantes.

<figure><img src="../../../.gitbook/assets/image (56).png" alt=""><figcaption></figcaption></figure>

En Arduino, la capacidad de interactuar con el mundo exterior es fundamental. Para lograrlo, utilizamos señales digitales, analógicas y PWM (Modulación por Ancho de Pulso). Estas señales nos permiten controlar componentes y sensores de manera precisa. En este artículo, exploraremos en detalle cada tipo de señal y cómo utilizar los pines digitales, analógicos y PWM en proyectos prácticos con Arduino.



## **Lista de componentes necesarios:**&#x20;

Antes de comenzar, asegurémonos de tener los siguientes componentes a mano:

<table><thead><tr><th width="254">COMPONENTE:</th><th width="372.3333333333333">IMAGEN:</th><th>CANTIDAD:</th></tr></thead><tbody><tr><td>Arduino Uno</td><td><img src="../../../.gitbook/assets/ARDUINO UNO (2).jpg" alt="" data-size="original"></td><td>1</td></tr><tr><td>Cable Arduino USB</td><td><img src="../../../.gitbook/assets/USB wire arduino.jpg" alt=""></td><td>1</td></tr><tr><td>Pulsador</td><td><img src="../../../.gitbook/assets/Pulsador.jpg" alt=""></td><td>1</td></tr><tr><td>Potenciometro</td><td><img src="../../../.gitbook/assets/potenciometro (1).jpg" alt=""></td><td>1</td></tr><tr><td>LED</td><td><img src="../../../.gitbook/assets/LEDs.jpg" alt=""></td><td>1</td></tr><tr><td>Resistencia de 10kΩ</td><td><img src="../../../.gitbook/assets/10K ohms.jpg" alt=""></td><td>1</td></tr><tr><td>Resistencia de 220Ω</td><td><img src="../../../.gitbook/assets/220 ohms.png" alt=""></td><td>1</td></tr><tr><td>Protoboard</td><td><img src="../../../.gitbook/assets/Protoboard.jpg" alt=""></td><td>1</td></tr><tr><td>Cables de conexión</td><td><img src="../../../.gitbook/assets/jumperwires.png" alt=""></td><td>1</td></tr></tbody></table>



## Señales Digitales: Encendido y Apagado Precisos

Las señales digitales son binarias, con dos posibles estados: HIGH (alto) o LOW (bajo), que representan 1 o 0, encendido o apagado, respectivamente. Estas señales son ideales para enviar y recibir datos binarios o controlar dispositivos que funcionan en base a estados discretos.

<figure><img src="../../../.gitbook/assets/image (7).png" alt=""><figcaption></figcaption></figure>

Los pines digitales pueden ser configurados como entradas o salidas digitales según nuestras necesidades. Cuando configuramos un pin digital como entrada, podemos leer el estado de la señal en ese pin. Por ejemplo, en el proyecto del botón pulsador, leemos el estado del botón mediante digitalRead(). Cuando configuramos un pin digital como salida, podemos enviar una señal en HIGH o LOW. Esto nos permite controlar dispositivos como LEDs, relés y motores, encendiéndolos o apagándolos según sea necesario.



## Proyecto 1: Botón Pulsador - Lectura de Valor y Control de 1 LED

En este proyecto, utilizaremos un botón pulsador para controlar un LED. Un botón pulsador es un interruptor momentáneo que se activa al ser presionado y se desactiva al soltarlo. Conectaremos el botón a un pin digital de Arduino y leeremos su estado para controlar el encendido y apagado de un LED.



<figure><img src="../../../.gitbook/assets/12 Led Pulse Button_bb.png" alt=""><figcaption></figcaption></figure>

1. Conecta un LED con el positivo (pata larga) al pin 13 en Arduino, y el negativo (pata corta) con una resistencia de 220 Ohms conectada al GND. _(Puedes usar las mismas conexiones del tutorial anterior)_.
2. Agrega el siguiente código al final del bloque `loop()` para encender o apagar el LED en función del estado del pulsador:

<pre class="language-arduino"><code class="lang-arduino">if (estadoPulsador == HIGH) {
  digitalWrite(13, HIGH);
<strong>} else {
</strong>  digitalWrite(13, LOW);
}
</code></pre>

* Utilizamos una estructura condicional `if-else` para verificar el estado del pulsador.
* Si el estado del pulsador es `HIGH` (es decir, está presionado), encendemos el LED utilizando `digitalWrite(13, HIGH)`.
* Si el estado del pulsador es `LOW` (es decir, no está presionado), apagamos el LED utilizando `digitalWrite(13, LOW)`.

<pre class="language-arduino"><code class="lang-arduino">void setup() {
  Serial.begin(9600);
  pinMode(2, INPUT);
}

void loop() {
  int estadoPulsador = digitalRead(2);
  Serial.println(estadoPulsador);
  delay(100);

  if (estadoPulsador == HIGH) {
  digitalWrite(13, HIGH);
  } else {
  digitalWrite(13, LOW);
<strong>  }
</strong>}
</code></pre>



## Señales Analógicas: Rango Continuo de Valores

A diferencia de las señales digitales, las señales analógicas pueden tener valores en un rango continuo. Los pines analógicos de Arduino tienen una resolución de 10 bits, lo que les permite representar valores de 0 a 1023. Estos pines son útiles para leer y escribir datos analógicos. En Arduino, los pines analógicos son los marcados con una letra (A) antes del número: A0, A1, A2, A3, A4 y A5. Estos se encuentran junto a los pines de alimentación eléctrica.

<figure><img src="../../../.gitbook/assets/image (42).png" alt=""><figcaption></figcaption></figure>



## PWM: Señales Digitales que parecen analógicas:

El PWM (Modulación por Ancho de Pulso) es una técnica utilizada en Arduino para generar señales digitales que se asemejan a señales analógicas. Funciona variando el ancho del pulso de una señal digital periódica, donde el ciclo de trabajo representa la proporción del tiempo que la señal está en estado alto. Arduino utiliza esta técnica en los pines PWM para controlar la intensidad de los LEDs, la velocidad de los motores y otras aplicaciones. Al ajustar el ciclo de trabajo, se puede controlar la intensidad o el voltaje de los componentes, simulando una señal analógica.

En Arduino, los pines PWM se pueden identificar mediante el símbolo "\~" junto a su número de pin en la placa. Por ejemplo, en la placa Arduino UNO, los pines PWM están marcados son los siguientes: 3, 5, 6, 9, 10 y 11. En otras placas de Arduino, como el Arduino Mega, hay más pines PWM disponibles.

<figure><img src="../../../.gitbook/assets/image (38).png" alt=""><figcaption></figcaption></figure>

Es importante destacar que aunque otros pines pueden generar señales digitales utilizando la función `digitalWrite()`, solo los pines PWM pueden generar señales analógicas simuladas mediante la función `analogWrite()`. Esta función acepta valores de 0 a 255, donde 0 representa el ciclo de trabajo del 0% (señal siempre apagada) y 255 representa el ciclo de trabajo del 100% (señal siempre encendida).



## Proyecto 2: Potenciómetro - Lectura de Valor y Control de 1 LED con PWM

En este proyecto, utilizaremos un potenciómetro para controlar la intensidad luminosa de un LED mediante la técnica de PWM. Un potenciómetro es un dispositivo de control giratorio que permite ajustar un valor resistivo en función de su posición. Aprenderemos cómo leer el valor del potenciómetro a través de un pin analógico y cómo utilizarlo para controlar la intensidad de un LED utilizando PWM.

<figure><img src="../../../.gitbook/assets/16 LED+potenciometro_bb.png" alt=""><figcaption></figcaption></figure>

### Conexiones:

1. Conecta el pin central del potenciómetro (conexión deslizante) a uno de los pines analógicos de Arduino, por ejemplo, A0.
2. Conecta un extremo del potenciómetro a 5V en Arduino.
3. Conecta el otro extremo del potenciómetro a GND (tierra) en Arduino.
4. Conecta el ánodo del LED (pata más larga) al pin 10 en Arduino.
5. Conecta el cátodo del LED (pata más corta) a través de la resistencia de 220Ω al GND en Arduino.



## Código:

```cpp
const int potPin = A0;
const int ledPin = 10;

void setup() {
  pinMode(ledPin, OUTPUT);
}

void loop() {
  int potValue = analogRead(potPin);
  int brightness = map(potValue, 0, 1023, 0, 255);
  analogWrite(ledPin, brightness);
}
```

* Configuramos el pin del LED como salida en el bloque setup() utilizando pinMode().
* En el bloque loop(), utilizamos la función analogRead() para leer el valor del potenciómetro en el pin analógico. Luego, utilizamos la función map() para mapear el valor leído (0-1023) al rango de brillo del LED (0-255) compatible con PWM.
* Finalmente, utilizamos la función analogWrite() para controlar la intensidad luminosa del LED mediante PWM. Cuanto mayor sea el valor del potenciómetro, más brillante será el LED.



En este artículo, hemos explorado las señales digitales, analógicas y PWM en Arduino. Hemos aprendido cómo utilizar los pines digitales como entradas y salidas, controlando componentes como botones pulsadores. También hemos visto cómo los pines analógicos nos permiten leer y escribir datos en un rango continuo, utilizando un potenciómetro como ejemplo. Además, hemos comprendido cómo utilizar la técnica de PWM para controlar la intensidad luminosa de un LED.

Recuerda que los pines digitales, analógicos y PWM de Arduino son herramientas poderosas para interactuar con el entorno y dar vida a tus proyectos creativos. ¡Experimenta y diviértete explorando las infinitas posibilidades que ofrecen estas señales en tus propios proyectos con Arduino!
