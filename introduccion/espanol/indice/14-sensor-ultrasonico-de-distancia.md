# 14 - Sensor ultrasónico de distancia

¡Bienvenidos nuevamente a POWAR STEAM! En este capítulo, nos sumergiremos en el emocionante mundo de los sensores ultrasónicos de distancia y aprenderemos cómo utilizarlos con Arduino. Estos sensores nos permiten medir la distancia entre el sensor y un objeto utilizando ondas ultrasónicas. En este capítulo, nos enfocaremos en los siguientes temas: qué es un sensor ultrasónico de distancia, instalación de bibliotecas necesarias, lectura y visualización de valores de distancia en el Serial Plotter, y control de un buzzer basado en la distancia.

Antes de comenzar con los proyectos, repasemos los componentes necesarios:

* Arduino Uno (o cualquier otra placa Arduino compatible)
* Sensor ultrasónico de distancia HC-SR04
* Buzzer pasivo
* Resistencia de 220 ohmios
* Cables de conexión
* Protoboard
* Jumper wires

Ahora que tenemos todos los componentes listos, ¡comencemos con el primer proyecto!

Proyecto 1: Lectura de distancia con el sensor ultrasónico

En este proyecto, conectaremos el sensor ultrasónico de distancia HC-SR04 a Arduino y leeremos los valores de distancia utilizando el monitor serial.

Paso 1: Conexión del sensor ultrasónico

Conectaremos el sensor ultrasónico de la siguiente manera:

* Conecta el pin VCC del sensor al pin 5V de Arduino.
* Conecta el pin GND del sensor al pin GND de Arduino.
* Conecta el pin Trig del sensor al pin digital 2 de Arduino.
* Conecta el pin Echo del sensor al pin digital 3 de Arduino.

Utiliza jumper wires para hacer las conexiones entre el sensor y Arduino. Puedes utilizar una protoboard para facilitar las conexiones y mantener el circuito ordenado.

Paso 2: Instalación de la biblioteca NewPing

Para utilizar el sensor ultrasónico HC-SR04, necesitaremos instalar una biblioteca en el IDE de Arduino. Una biblioteca es como un libro de instrucciones que nos proporciona funciones ya escritas para realizar ciertas tareas. En este caso, la biblioteca "NewPing" nos facilitará la interacción con el sensor ultrasónico.

Sigue estos pasos para instalar la biblioteca "NewPing":

1. Abre el IDE de Arduino.
2. Ve a "Sketch" en la barra de menú y selecciona "Incluir Biblioteca" > "Gestor de Bibliotecas".
3. En el cuadro de búsqueda, escribe "NewPing" y selecciona la biblioteca "NewPing" de Tim Eckel.
4. Haz clic en "Instalar" y espera a que se complete la instalación.

Paso 3: Lectura y visualización de valores de distancia en el Serial Plotter

Ahora que hemos instalado la biblioteca necesaria, podemos comenzar a programar. Abre un nuevo sketch en el IDE de Arduino y comienza importando la biblioteca "NewPing". Esto le dirá a Arduino que queremos utilizar las funciones que se encuentran en esa biblioteca.

```arduino
arduinoCopy code#include <NewPing.h>
```

A continuación, definiremos los pines de conexión para el sensor ultrasónico y crearemos una instancia del sensor utilizando la clase "NewPing". En la función "setup", inicializaremos la comunicación con el monitor serial, que es una herramienta que nos permite ver los valores que enviamos desde Arduino.

```arduino
arduinoCopy code#define TRIGGER_PIN 2
#define ECHO_PIN 3
#define MAX_DISTANCE 200

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

void setup() {
  Serial.begin(9600);
}
```

En la función "loop", utilizaremos el método ".ping\_cm()" de la instancia del sensor y enviaremos ese valor al monitor serial para visualizarlo.

```arduino
arduinoCopy codevoid loop() {
  delay(50);
  unsigned int distance = sonar.ping_cm();
  Serial.println(distance);
}
```

Cada vez que se ejecuta la función "loop", el sensor enviará una onda ultrasónica y medirá el tiempo que tarda en recibir el eco. Luego, convertirá ese tiempo en una distancia en centímetros y la enviará al monitor serial.

Conecta tu Arduino a la computadora, selecciona la placa y el puerto correctos en el IDE de Arduino, y carga el programa en Arduino. Abre el monitor serial y podrás ver los valores de distancia que se están leyendo. Prueba mover objetos cerca y lejos del sensor para ver cómo cambian los valores.

Proyecto 2: Control de un buzzer basado en la distancia

Una vez que hemos aprendido a leer y visualizar los valores de distancia, pasemos al segundo proyecto: controlar un buzzer basado en la distancia medida. En este proyecto, utilizaremos el buzzer para producir diferentes tonos de sonido dependiendo de la distancia medida.

Paso 1: Conexión del buzzer

Conectaremos el buzzer pasivo a Arduino utilizando una resistencia de 220 ohmios para limitar la corriente. Asegúrate de conectar el pin positivo del buzzer a un pin digital de salida de Arduino y el pin negativo a GND.

Paso 2: Explicación del buzzer

Antes de comenzar, es importante entender qué es un buzzer. Un buzzer es un dispositivo que produce sonidos o tonos de forma controlada. Al aplicarle una señal eléctrica, genera vibraciones que se traducen en sonido. En nuestro caso, utilizaremos el buzzer para producir diferentes tonos de sonido en función de la distancia medida por el sensor ultrasónico.

Paso 3: Programación del control del buzzer

Primero, agregaremos la definición del pin del buzzer al principio del sketch y configuraremos ese pin como una salida en la función "setup". Luego, en la función "loop", leeremos la distancia utilizando el sensor ultrasónico y utilizaremos declaraciones "if" para determinar el tono del buzzer en función de la distancia medida.

```arduino
arduinoCopy codeint buzzerPin = 4;

void setup() {
  pinMode(buzzerPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  delay(50);
  unsigned int distance = sonar.ping_cm();

  if (distance < 10) {
    tone(buzzerPin, 1000);
  } else if (distance >= 10 && distance < 20) {
    tone(buzzerPin, 500);
  } else {
    tone(buzzerPin, 200);
  }

  Serial.println(distance);
}
```

En este código, hemos agregado la definición del pin del buzzer y configurado ese pin como una salida en la función "setup". Luego, en la función "loop", leemos la distancia utilizando el sensor ultrasónico y utilizamos declaraciones "if" para determinar el tono del buzzer en función de la distancia medida. En este caso, si la distancia es menor que 10 cm, el buzzer emitirá un tono de 1000 Hz. Si la distancia está entre 10 y 20 cm, emitirá un tono de 500 Hz. De lo contrario, emitirá un tono de 200 Hz.

Conecta el buzzer al pin correcto en Arduino, carga el programa en Arduino y observa cómo el tono del buzzer cambia a medida que mueves objetos cerca y lejos del sensor ultrasónico.

¡Y eso es todo para este capítulo! Esperamos que hayas disfrutado aprendiendo sobre los sensores ultrasónicos de distancia y cómo utilizarlos con Arduino. En el próximo capítulo, exploraremos nuevos proyectos emocionantes. ¡Nos vemos pronto en POWAR STEAM!
