---
description: Explorando el mundo de los microcontroladores y Arduino
---

# 1 - ¿Qué es Arduino?

<figure><img src="https://images.unsplash.com/photo-1553406830-ef2513450d76?crop=entropy&#x26;cs=srgb&#x26;fm=jpg&#x26;ixid=M3wxOTcwMjR8MHwxfHNlYXJjaHw1fHxhcmR1aW5vfGVufDB8fHx8MTY4NDYxNTI3OXww&#x26;ixlib=rb-4.0.3&#x26;q=85" alt=""><figcaption><p>Arduino UNO</p></figcaption></figure>

En el emocionante mundo de la electrónica y la programación, existen varios tipos de microcontroladores que nos permiten crear proyectos asombrosos. Además de Arduino, hay otras placas populares como micro:bit, ESP32, ESP8266 y Raspberry Pi, cada una con sus propias características y capacidades únicas. En este artículo, vamos a explorar un poco más sobre estos microcontroladores y su relación con Arduino.

Comencemos con micro:bit, una placa diseñada especialmente para introducir a niños y jóvenes en el mundo de la programación. Micro:bit es compacta y cuenta con una matriz de LEDs, botones, sensores y conexiones para ampliar sus capacidades. Es como tener un pequeño ayudante que te permite crear proyectos divertidos y aprender a programar de manera interactiva. Puedes pensar en micro:bit como el hermano menor de Arduino, enfocado en el aprendizaje y la experimentación.

<figure><img src="https://images.unsplash.com/photo-1680731253572-92052349dd21?crop=entropy&#x26;cs=srgb&#x26;fm=jpg&#x26;ixid=M3wxOTcwMjR8MHwxfHNlYXJjaHwyfHxtaWNybyUyMGJpdHxlbnwwfHx8fDE2ODQ2NTY1ODl8MA&#x26;ixlib=rb-4.0.3&#x26;q=85" alt=""><figcaption><p>Micro.Bit</p></figcaption></figure>

Ahora hablemos de ESP32 y ESP8266, dos microcontroladores ampliamente utilizados para proyectos de IoT (Internet of Things). Estas placas tienen capacidades de conexión Wi-Fi y Bluetooth, lo que les permite comunicarse y controlar dispositivos a través de Internet. Puedes imaginar a ESP32 y ESP8266 como los superhéroes de la conectividad, capaces de llevar tus proyectos al siguiente nivel al permitirles interactuar con el mundo digital.

<figure><img src="https://images.unsplash.com/photo-1634452015397-ad0686a2ae2d?crop=entropy&#x26;cs=srgb&#x26;fm=jpg&#x26;ixid=M3wxOTcwMjR8MHwxfHNlYXJjaHw3fHxFU1AzMnxlbnwwfHx8fDE2ODQ2NTY2NTh8MA&#x26;ixlib=rb-4.0.3&#x26;q=85" alt=""><figcaption><p>ESP32</p></figcaption></figure>

Otro jugador importante en el mundo de los microcontroladores es Raspberry Pi. A diferencia de Arduino y los otros microcontroladores mencionados, Raspberry Pi es una computadora completa en una placa pequeña. Puedes instalar un sistema operativo y utilizarlo para realizar una amplia gama de tareas, desde proyectos de automatización hasta servidores web caseros. Raspberry Pi es como tener una mini computadora en tus manos, lista para hacer realidad tus ideas más ambiciosas.

<figure><img src="https://images.unsplash.com/photo-1610812387871-806d3db9f5aa?crop=entropy&#x26;cs=srgb&#x26;fm=jpg&#x26;ixid=M3wxOTcwMjR8MHwxfHNlYXJjaHw0fHxyYXNwYmVycnklMjBwaXxlbnwwfHx8fDE2ODQ2NTY3Mjl8MA&#x26;ixlib=rb-4.0.3&#x26;q=85" alt=""><figcaption><p>Raspberry Pi</p></figcaption></figure>

Ahora, hablemos de Arduino y su historia fascinante. Arduino nació como un proyecto en Italia en 2005 con el objetivo de crear una plataforma de hardware y software de código abierto para facilitar la creación de proyectos electrónicos. La idea era hacer que la electrónica y la programación fueran accesibles para todos, sin importar su nivel de experiencia. Desde entonces, Arduino se ha convertido en un fenómeno global, con una comunidad activa de makers y una amplia variedad de placas disponibles para adaptarse a diferentes necesidades.

<figure><img src="https://images.unsplash.com/photo-1631378297854-185cff6b0986?crop=entropy&#x26;cs=srgb&#x26;fm=jpg&#x26;ixid=M3wxOTcwMjR8MHwxfHNlYXJjaHw0fHxhcmR1aW5vfGVufDB8fHx8MTY4NDYxNTI3OXww&#x26;ixlib=rb-4.0.3&#x26;q=85" alt=""><figcaption></figcaption></figure>

Arduino es conocido por su filosofía de código abierto, lo que significa que su diseño y especificaciones son públicas y cualquiera puede utilizarlos para fabricar sus propias placas compatibles con Arduino. Esto ha dado lugar a una amplia gama de fabricantes y modelos de placas de Arduino en el mercado. Algunos de los fabricantes más populares son Arduino LLC y Arduino SRL (anteriormente conocida como Smart Projects). Ambas empresas tienen licencia para fabricar placas Arduino y han contribuido significativamente al desarrollo y la evolución de la plataforma.



## Arduino UNO - Características:

Arduino ofrece una amplia variedad de pines que facilitan la interacción con componentes electrónicos. A continuación, veremos los principales tipos de pines y otras conexiones importantes de Arduino:

<figure><img src="../../../.gitbook/assets/image (47).png" alt=""><figcaption></figcaption></figure>

1. **Pines Digitales y Analógicos:** Los pines digitales trabajan con señales binarias, es decir, pueden tener dos estados: HIGH o LOW. Estos pines son ideales para enviar y recibir datos digitales o controlar dispositivos con estados discretos. Por otro lado, los pines analógicos permiten leer y escribir datos en un rango continuo, proporcionando una resolución de 10 bits para representar valores de 0 a 1023.
2. **Pines PWM (Modulación por Ancho de Pulso):** Los pines PWM permiten generar señales digitales que simulan señales analógicas al variar el ancho del pulso. Esta técnica se utiliza comúnmente para controlar la intensidad luminosa de los LEDs, la velocidad de los motores y otras aplicaciones que requieren una señal analógica simulada.
3. **Pines I2C:** Arduino también cuenta con pines I2C, que son utilizados para la comunicación en serie con otros dispositivos. Estos pines, conocidos como SDA (Serial Data) y SCL (Serial Clock), permiten la conexión de sensores y actuadores que utilizan el protocolo I2C, lo que simplifica la comunicación con varios dispositivos en un mismo bus.
4. **Conector de 9V:** Algunas placas Arduino tienen un conector de 9V que permite la conexión de una fuente de alimentación externa para suministrar energía a la placa. Esto es especialmente útil cuando se trabaja con proyectos que requieren una mayor corriente o cuando se utilizan componentes que no pueden ser alimentados directamente por el Arduino.
5. **Cable USB:** Arduino se puede conectar a un ordenador utilizando un cable USB, lo que permite cargar programas en la placa y establecer comunicación bidireccional. Además de transferir datos, el cable USB también proporciona energía al Arduino, lo que lo hace práctico y portátil.

Esperamos que esta introducción a las características y pines de Arduino te haya resultado útil. La versatilidad de Arduino y sus conexiones adicionales te permiten explorar una amplia gama de proyectos electrónicos. ¡No dudes en experimentar y dejar volar tu creatividad!

¡Disfruta de tu viaje en el apasionante mundo de la electrónica y la programación con Arduino!
