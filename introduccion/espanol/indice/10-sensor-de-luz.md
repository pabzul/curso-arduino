# 10 - Sensor de Luz

¡Explorando el Mundo de los Sensores de Luz con Arduino!

En el fascinante mundo de la electrónica, existen dispositivos llamados sensores que nos permiten medir y detectar diferentes variables del entorno. Uno de los sensores más comunes es el sensor de luz, también conocido como fotoresistor o LDR (Light Dependent Resistor). En este tutorial, te guiaré a través de los pasos necesarios para utilizar un sensor de luz con Arduino y realizar diferentes proyectos. ¡Prepárate para adentrarte en el mundo de los sensores y descubrir cómo funcionan!

¿Qué es un Sensor de Luz y cómo funciona? Un sensor de luz es un componente electrónico que varía su resistencia en función de la intensidad luminosa del entorno. Está compuesto por un material fotosensible que cambia su conductividad cuando se expone a la luz. Cuando hay poca luz, la resistencia del sensor aumenta, mientras que en condiciones de alta luminosidad, su resistencia disminuye. Esto permite que el sensor pueda detectar y medir los niveles de luz ambiente.

###

### **Lista de Materiales:**&#x20;

Para llevar a cabo los proyectos con el sensor de luz, necesitarás los siguientes materiales:

<table><thead><tr><th width="254">COMPONENTE:</th><th width="372.3333333333333">IMAGEN:</th><th align="center">CANTIDAD:</th></tr></thead><tbody><tr><td>Arduino Uno</td><td><img src="../../../.gitbook/assets/ARDUINO UNO (2).jpg" alt="" data-size="original"></td><td align="center">1</td></tr><tr><td>Cable Arduino USB</td><td><img src="../../../.gitbook/assets/USB wire arduino (1).jpg" alt=""></td><td align="center">1</td></tr><tr><td>Sensor de luz (fotoresistor o LDR)</td><td><img src="../../../.gitbook/assets/LDR (1).jpg" alt=""></td><td align="center">1</td></tr><tr><td>LED</td><td><img src="../../../.gitbook/assets/LEDs.jpg" alt=""></td><td align="center">1</td></tr><tr><td>Resistencia de 10kΩ</td><td><img src="../../../.gitbook/assets/10K ohms.jpg" alt=""></td><td align="center">1</td></tr><tr><td>Resistencia de 220Ω</td><td><img src="../../../.gitbook/assets/220 ohms.png" alt=""></td><td align="center">1</td></tr><tr><td>Protoboard</td><td><img src="../../../.gitbook/assets/Protoboard.jpg" alt=""></td><td align="center">1</td></tr><tr><td>Cables de conexión</td><td><img src="../../../.gitbook/assets/jumperwires.png" alt=""></td><td align="center">1</td></tr></tbody></table>



Ahora que conocemos los materiales necesarios, vamos a comenzar con el primer tutorial.

###

## **Proyecto 1:** Lectura del Sensor de Luz y Monitor Serial

En este primer proyecto conectaremos un sensor de luz LDR al arduino, el cuál varía su resistencia según la cantidad de luz que recibe, y nos dará unos valores entre 0 y 1023. Estos valores los mediremos por uno de nuestros pines analógicos, y los visualizaremos en el monitor serial.

### Paso 1: Conexiones del Sensor de Luz Realicemos las conexiones en el protoboard siguiendo estos pasos:

<figure><img src="../../../.gitbook/assets/5.3 - LDR Read_bb (2).png" alt=""><figcaption></figcaption></figure>

1. Conecta un extremo de la resistencia de 10k Ohms a la pata del sensor de luz.
2. Conecta el otro extremo de la resistencia a la pata GND (tierra) del Arduino.
3. Conecta el extremo opuesto del sensor de luz a la pata A0 (o cualquier otra entrada analógica) del Arduino.
4. Conecta el Arduino a tu ordenador mediante el cable USB.

###

### Paso 2: Código para Leer el Sensor de Luz y Visualizar los Datos en el Monitor Serial

Abre el entorno de desarrollo Arduino IDE y escribe el siguiente código:

```arduino
const int sensorPin = A0;   // Pin del sensor de luz

void setup() {
  pinMode(sensorPin, INPUT);   // Configurar el pin del sensor como entrada
  Serial.begin(9600);          // Inicializar la comunicación serial
}

void loop() {
  int lightValue = analogRead(sensorPin);  // Leer el valor de luz del sensor

  Serial.print("Luz: ");
  Serial.println(lightValue);

  delay(100);                              // Retardo de 100 milisegundos
}
```

Antes de cargar el código en tu placa Arduino, es importante entender que `analogRead(sensorPin)` lee el valor analógico del sensor de luz. Este valor se almacena en la variable `lightValue`, que luego se imprime en el Monitor Serial mediante `Serial.println(lightValue)`.

###

### Visualización de los Datos en el Monitor Serial

(IMAGEN DE LECTURA DEL MONITOR SERIAL)

Una vez que hayas cargado el código en tu Arduino, abre el Monitor Serial haciendo clic en el icono correspondiente en la barra de herramientas del Arduino IDE (o a través de "Herramientas" > "Monitor Serie"). Asegúrate de seleccionar la velocidad de baudios correcta (9600 en este caso).

Ahora, deberías ver los valores de luz que se leen del sensor en el Monitor Serial. Observa cómo cambian los valores a medida que varías la intensidad de la luz sobre el sensor.

###

## Proyecto 2: Control de Intensidad de un LED con el Sensor de Luz

Para este segundo proyecto, lo que haremos será que el sensor de luz controle directamente la intencidad de otro LED, haciendo que brille mucho cuando haya poca luz, o que brille poco o nada cuando haya abundante luz.

### Paso 1: Conexiones del Sensor de Luz y el LED Agreguemos el LED y una resistencia adicional al circuito. Realicemos las siguientes conexiones:

<figure><img src="../../../.gitbook/assets/6 - LDR 1 LED.png" alt=""><figcaption></figcaption></figure>

1. Conecta una pata del LED (el lado más largo, llamado ánodo) al pin digital 9 del Arduino.
2. Conecta el otro extremo de la resistencia de 220 Ohms al ánodo del LED.
3. Conecta el extremo opuesto de la resistencia al GND (tierra) del Arduino.

###

### Paso 2: Código para Controlar la Intensidad del LED según la Luz Ambiente

Actualicemos el código anterior para que ahora controle la intensidad del LED en función de la luz ambiente. Reemplaza el código anterior por el siguiente:

```arduino
const int sensorPin = A0;     // Pin del sensor de luz
const int ledPin = 9;        // Pin del LED

void setup() {
  pinMode(sensorPin, INPUT);    // Configurar el pin del sensor como entrada
  pinMode(ledPin, OUTPUT);      // Configurar el pin del LED como salida
  Serial.begin(9600);           // Inicializar la comunicación serial
}

void loop() {
  int lightValue = analogRead(sensorPin);                             // Leer el valor de luz del sensor
  int brightness = map(lightValue, 0, 1023, 0, 255);                  // Mapear el rango de valores del sensor al rango del LED

  analogWrite(ledPin, brightness);                                    // Controlar la intensidad del LED según la luz

  Serial.print("Luz: ");
  Serial.println(lightValue);

  delay(100);                                                        // Retardo de 100 milisegundos
}
```

En este nuevo código, utilizamos la función `map()` para mapear los valores del sensor de luz (0-1023) al rango de valores del LED (0-255). Esto nos permite controlar la intensidad del LED proporcionalmente a la intensidad de la luz ambiente.

###

### Paso 3: Observar el Control de Intensidad del LED

Carga el código en tu Arduino y observa cómo la intensidad del LED varía según la luz ambiente. Prueba cubriendo el sensor de luz con tus manos o acercando una fuente de luz para ver cómo reacciona el LED. ¡Experimenta y Diviértete!

Ahora que has completado ambos tutoriales, has aprendido cómo leer los datos del sensor de luz y controlar la intensidad de un LED según la luz ambiente. Estos son solo los primeros pasos en el fascinante mundo de los sensores y Arduino.

Recuerda que puedes explorar y experimentar con diferentes proyectos utilizando el sensor de luz. Puedes crear sistemas de iluminación automática, seguidores de luz solar y mucho más.

¡Deja volar tu imaginación y diviértete creando tus propios dispositivos electrónicos!
