# 13 - Sensor de temperatura y humedad DHT11

¡Bienvenidos de nuevo al blog del curso de Arduino de POWAR STEAM! En este capítulo, vamos a explorar el uso del sensor de temperatura y humedad relativa del aire DHT11 con Arduino. Antes de empezar, es importante destacar que existen diferentes tipos de sensores de humedad y temperatura, pero en este tutorial nos centraremos en el sensor DHT11. Aprenderemos cómo conectar el sensor a Arduino, instalar las bibliotecas necesarias, leer y visualizar los valores de temperatura y humedad, y controlar un motor DC con una hélice de ventilador basado en la temperatura medida. ¡Comencemos!

Introducción al sensor de temperatura y humedad relativa del aire: El sensor de temperatura y humedad relativa del aire DHT11 es un dispositivo muy común y ampliamente utilizado para medir la temperatura y humedad en diferentes entornos. Proporciona lecturas precisas y es fácil de utilizar con Arduino. Es importante mencionar que existen otros sensores disponibles en el mercado, como el DHT22, que ofrecen mayor precisión y rango de medición, pero en este tutorial nos enfocaremos en el DHT11.



## **Lista de Materiales:**&#x20;

Para empezar, necesitaremos los siguientes componentes:

<table><thead><tr><th width="254">COMPONENTE:</th><th width="372.3333333333333">IMAGEN:</th><th align="center">CANTIDAD:</th></tr></thead><tbody><tr><td>Arduino Uno (o similar)</td><td><img src="../../../.gitbook/assets/ARDUINO UNO (2).jpg" alt="" data-size="original"></td><td align="center">1</td></tr><tr><td>Cable Arduino USB</td><td><img src="../../../.gitbook/assets/USB wire arduino (1).jpg" alt=""></td><td align="center">1</td></tr><tr><td>DHT11 (sensor humedad y temperatura relativa)</td><td><img src="../../../.gitbook/assets/image (43).png" alt=""></td><td align="center">1</td></tr><tr><td>Protoboard</td><td><img src="../../../.gitbook/assets/Protoboard.jpg" alt=""></td><td align="center">1</td></tr><tr><td>Cables de conexión</td><td><img src="../../../.gitbook/assets/jumperwires.png" alt=""></td><td align="center">1</td></tr></tbody></table>



### Conexión del sensor de temperatura y humedad a Arduino:

Con estos simples pasos, habrás conectado correctamente el sensor DHT11 al Arduino, y podrás utilizarlo para medir la temperatura y la humedad en tus proyectos.

<figure><img src="../../../.gitbook/assets/10.- - DHT11_bb.png" alt=""><figcaption></figcaption></figure>

1. Conecta el pin VCC del sensor al pin 5V de Arduino.
2. Conecta el pin GND del sensor al pin GND de Arduino.
3. Conecta el pin de datos (Data o Sig) del sensor al pin digital 2 de Arduino.

No se requiere una resistencia adicional en este caso, ya que el módulo del sensor DHT11 incluye una resistencia de pull-up interna en el pin de datos. Esta resistencia garantiza la estabilidad de la señal. Según el fabricante, los sensores de humedad **pueden varias en el orden de sus patas, o incluso en algunos llamar SIG o S (de signal) a la pata de Data (dat).**

<figure><img src="../../../.gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

### Instalación de bibliotecas necesarias:&#x20;

Antes de poder utilizar el sensor DHT11, necesitaremos instalar una biblioteca en el entorno de desarrollo de Arduino. Una biblioteca es un conjunto de código predefinido que nos permite utilizar funciones específicas sin tener que escribir todo el código desde cero. Sigue estos pasos para instalar la biblioteca necesaria:

* Abre el IDE de Arduino.
* Ve a "Sketch" -> "Include Library" -> "Manage Libraries" (en la versión antigua del IDE). O has clic en el botón de bibliotecas en el menú que está a la izquierda.
* En el campo de búsqueda, escribe "DHT".
* Aparecerán varias bibliotecas relacionadas con el sensor DHT. Busca "DHT sensor library" de Adafruit y haz clic en "Install".
* Una vez instalada la biblioteca, cierra el administrador de bibliotecas.
* Ahora que tenemos todo configurado, vamos a leer los valores de temperatura y humedad del sensor y mostrarlos en el monitor serial.&#x20;

<figure><img src="../../../.gitbook/assets/image (49).png" alt=""><figcaption></figcaption></figure>



Aquí tienes un código de ejemplo para hacerlo:

```cpp
#include <DHT.h>          // Incluye la biblioteca DHT.h

#define DHTPIN 2          // Pin de datos del sensor DHT11
#define DHTTYPE DHT11     // Tipo de sensor DHT11

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);    // Inicia puerto serial
  dht.begin();
}

void loop() {
  delay(2000);           // Espera 2 segundos entre lecturas

  float temperature = dht.readTemperature();  // Crea una flotante y asigna la lectura del sensor
  float humidity = dht.readHumidity();        // Crea una flotante y asigna la lectura del sensor

  Serial.print("Temperatura: ");     // Imprime en el puerto serial "Temperatura:"
  Serial.print(temperature);
  Serial.println(" °C");

  Serial.print("Humedad: ");
  Serial.print(humidity);
  Serial.println(" %");
}
```

Este código inicializa el sensor DHT11, inicia la comunicación con el monitor serial y realiza la lectura de temperatura y humedad cada 2 segundos. Los valores se imprimen en el monitor serial, que puedes abrir haciendo clic en el ícono correspondiente en la barra de herramientas del IDE de Arduino.

### Control de un motor DC basado en la temperatura medida:

En este proyecto, vamos a utilizar los valores de temperatura medidos por el sensor DHT11 para controlar un motor DC con una hélice de ventilador. Si la temperatura supera un umbral predefinido, el motor se activará y la hélice comenzará a girar, creando un flujo de aire.

#### Para este proyecto, necesitarás:

* Motor DC con hélice de ventilador
* Transistor NPN (por ejemplo, el transistor BC547)
* Resistencia de 220 ohmios
* Diodo de protección (por ejemplo, el diodo 1N4007)
* Cables de conexión

####

#### La conexión del motor DC a Arduino es la siguiente:

1. Conecta el pin positivo del motor al pin VIN de Arduino (o a una fuente de alimentación externa).
2. Conecta el pin negativo del motor al colector del transistor.
3. Conecta el emisor del transistor a GND de Arduino.
4. Conecta la resistencia de 220 ohmios entre la base del transistor y un pin de salida digital de Arduino (por ejemplo, pin 9).
5. Conecta el diodo de protección entre los pines positivo y negativo del motor, con la banda del diodo en el pin positivo.

Ahora, modifica el código anterior para agregar el control del motor basado en la temperatura medida. Aquí tienes un ejemplo de cómo hacerlo:

```cpp
#include <DHT.h>

#define DHTPIN 2          // Pin de datos del sensor DHT11
#define DHTTYPE DHT11     // Tipo de sensor DHT11
#define fanPin 9          // Pin de salida para controlar el motor DC

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  dht.begin();
  pinMode(fanPin, OUTPUT);
}

void loop() {
  delay(2000);  // Espera 2 segundos entre lecturas

  float temperature = dht.readTemperature();

  Serial.print("Temperatura: ");
  Serial.print(temperature);
  Serial.println(" °C");

  if (temperature >= 25) {
    digitalWrite(fanPin, HIGH);  // Encender el motor si la temperatura es mayor o igual a 25 °C
  } else {
    digitalWrite(fanPin, LOW);   // Apagar el motor si la temperatura es menor a 25 °C
  }
}
```

En este ejemplo, utilizamos el pin 9 de Arduino para controlar el motor DC. Si la temperatura medida es mayor o igual a 25 °C, el motor se encenderá; de lo contrario, se apagará.

¡Y eso es todo! Ahora tienes los conocimientos necesarios para utilizar el sensor de temperatura y humedad DHT11 con Arduino, leer y visualizar los valores de temperatura y humedad, y controlar un motor DC basado en la temperatura medida. Te invitamos a experimentar y explorar cómo alterar la temperatura del sensor para ver cómo se activa el ventilador hecho con el motor DC y la hélice.

¡Diviértete y sigue aprendiendo en el apasionante mundo de Arduino con POWAR STEAM!
