# 4 - Introducción al voltaje, Corriente y Resistencia



¡Bienvenidos a nuestro blog de POWAR STEAM! En este artículo, vamos a adentrarnos en el fascinante mundo de la electricidad y explorar los conceptos básicos que sentarán las bases para comprender este campo tan emocionante. En particular, nos enfocaremos en los siguientes temas: voltaje, corriente y resistencia, y corriente AC y DC.

##

## **Voltaje, Corriente y Resistencia**

**Comencemos por entender tres conceptos fundamentales: voltaje, corriente y resistencia.** Estos conceptos son esenciales para comprender cómo funciona la electricidad y cómo se comportan los circuitos eléctricos.

<figure><img src="../../../.gitbook/assets/image (63).png" alt="Voltaje, corriente y resistencia"><figcaption><p><a href="https://transistores.info/wp-content/uploads/2021/01/representacion-humoristica-de-la-ley-de-ohm-768x304.jpg">https://transistores.info/wp-content/uploads/2021/01/representacion-humoristica-de-la-ley-de-ohm-768x304.jpg</a></p></figcaption></figure>

El **voltaje** es la fuerza impulsora que impulsa el flujo de corriente eléctrica en un circuito. Es similar a la presión en una tubería de agua. Se mide en voltios (V) y representa la diferencia de potencial eléctrico entre dos puntos de un circuito. El voltaje es esencial para proporcionar la energía necesaria para que los electrones se muevan a través de un conductor y produzcan corriente eléctrica.

La **corriente** es el flujo de carga eléctrica a través de un conductor. Podemos imaginarla como el flujo de agua en una tubería. La corriente se mide en amperios (A) y representa la cantidad de carga que fluye por unidad de tiempo. La corriente eléctrica es el resultado del movimiento de los electrones en un circuito, impulsados por el voltaje.

Por último, la **resistencia** es la oposición al flujo de corriente y se mide en ohmios (Ω). La resistencia puede considerarse como un estrechamiento en la tubería que dificulta el paso del agua. En un circuito eléctrico, la resistencia limita la cantidad de corriente que puede fluir. Los componentes electrónicos llamados resistencias se utilizan para controlar la cantidad de corriente en un circuito y proteger otros componentes sensibles.

<figure><img src="../../../.gitbook/assets/image (52).png" alt="Voltaje, corriente y resistencia"><figcaption><p><a href="https://1.bp.blogspot.com/-4JjzrGaRuvQ/Wgj3Sy7VXmI/AAAAAAAAALo/7snKxywLALU419ZjMB8fsp-QVJOl2svTgCLcBGAs/s1600/Agua%2BOhm.png">https://1.bp.blogspot.com/-4JjzrGaRuvQ/Wgj3Sy7VXmI/AAAAAAAAALo/7snKxywLALU419ZjMB8fsp-QVJOl2svTgCLcBGAs/s1600/Agua%2BOhm.png</a></p></figcaption></figure>



## **AC y DC: Corriente Alterna y Corriente Continua**

<figure><img src="../../../.gitbook/assets/image (53).png" alt="AC DC corriente current"><figcaption><p><a href="https://academiaplay.es/wp-content/uploads/2016/06/2.jpg">https://academiaplay.es/wp-content/uploads/2016/06/2.jpg</a></p></figcaption></figure>

Ahora, adentrémonos en la diferencia entre corriente alterna (AC) y corriente continua (DC). Estos términos se refieren a la forma en que fluye la corriente eléctrica en un circuito.

<figure><img src="../../../.gitbook/assets/image (34).png" alt=""><figcaption></figcaption></figure>

**La corriente alterna (AC)** es aquella en la que la dirección de flujo de la corriente cambia constantemente en un patrón periódico, lo que significa que en algunos momentos el cable rojo es positivo y el negro negativo, y en otros momentos se invierten oscilantemente. La corriente alterna es la corriente que utilizamos en nuestros hogares para la iluminación y los electrodomésticos, es la que sale por los tomacorrientes de nuestras casas. Se genera en centrales eléctricas y se distribuye a través de redes eléctricas. La corriente alterna es especialmente útil para la transmisión de energía a largas distancias.

<figure><img src="../../../.gitbook/assets/image (54).png" alt=""><figcaption></figcaption></figure>

**La corriente continua (DC)** es aquella en la que el flujo de corriente es constante en una dirección, por lo que siempre se mueven en el mismo sentido, de positivo a negativo. La corriente continua es la corriente que proviene de baterías y fuentes de alimentación. Es ampliamente utilizada en dispositivos electrónicos portátiles y circuitos electrónicos que requieren un flujo de corriente constante y unidireccional.

<figure><img src="../../../.gitbook/assets/image (59).png" alt=""><figcaption></figcaption></figure>

En el próximo artículo, continuaremos explorando los conceptos básicos de electricidad con la **ley de Ohm, los colores de las resistencias** y el **cálculo de resistencias en circuitos**. Estos conceptos nos permitirán comprender las relaciones entre voltaje, corriente y resistencia, y cómo calcular los valores adecuados de resistencias en un circuito.

¡No te pierdas el próximo artículo donde expandiremos tus conocimientos en el apasionante mundo de la electricidad! Si tienes alguna pregunta o algún tema específico que te gustaría que abordemos, déjanos un comentario y estaremos encantados de ayudarte.

¡Sigue aprendiendo y explorando el maravilloso mundo de la electrónica y la electricidad!
