# 2 - Arduino IDE: Descarga e instalación

<figure><img src="https://images.unsplash.com/photo-1591439657848-9f4b9ce436b9?crop=entropy&#x26;cs=srgb&#x26;fm=jpg&#x26;ixid=M3wxOTcwMjR8MHwxfHNlYXJjaHwyfHxhcmR1aW5vJTIwSURFfGVufDB8fHx8MTY4NDY2ODYxMnww&#x26;ixlib=rb-4.0.3&#x26;q=85" alt=""><figcaption></figcaption></figure>

¡Bienvenidos de nuevo, creadores de POWAR STEAM! En este emocionante capítulo, vamos a adentrarnos en el mundo del software Arduino IDE y aprender cómo descargarlo e instalarlo en nuestro ordenador. Este software será nuestra herramienta principal para programar y cargar nuestros proyectos en las placas Arduino. ¡Así que asegúrate de tener tu capa de superhéroe lista, porque vamos a empezar!



## **Lista de componentes necesarios:**&#x20;

Antes de comenzar, asegurémonos de tener los siguientes componentes a mano para nuestros proyectos:

<table><thead><tr><th width="254">COMPONENTE:</th><th width="372.3333333333333">IMAGEN:</th><th align="center">CANTIDAD:</th></tr></thead><tbody><tr><td>Arduino Uno</td><td><img src="../../../.gitbook/assets/ARDUINO UNO (2).jpg" alt="" data-size="original"></td><td align="center">1</td></tr><tr><td>Cable Arduino USB</td><td><img src="../../../.gitbook/assets/USB wire arduino (1).jpg" alt=""></td><td align="center">1</td></tr></tbody></table>



## **Descarga del Arduino IDE**

Para comenzar nuestra aventura, necesitamos tener el Arduino IDE en nuestro ordenador. Sigue estos sencillos pasos para descargarlo:

{% embed url="https://www.arduino.cc/" %}

1.  Abre tu navegador web y dirígete a la página oficial de Arduino en arduino.cc.\


    <figure><img src="../../../.gitbook/assets/image (22).png" alt=""><figcaption></figcaption></figure>


2.  Busca la sección de descargas y haz clic en el enlace que corresponda a tu sistema operativo (Windows, Mac o Linux). \


    <figure><img src="../../../.gitbook/assets/image (45).png" alt=""><figcaption></figcaption></figure>


3.  En la siguiente pantalla, decide si quieres donar a la fundación Arduino o simplemente descargar de forma gratuita haciendo clic en "Just Download" para iniciar la descarga.\


    <figure><img src="../../../.gitbook/assets/image (66).png" alt=""><figcaption></figcaption></figure>

¡Recuerda que siempre es mejor utilizar la versión más reciente para aprovechar todas las mejoras y características!



## **Instalación del Arduino IDE**

Ahora que hemos descargado el Arduino IDE, es hora de instalarlo en nuestro ordenador. Sigue estos pasos para una instalación exitosa:

1. Abre el archivo de instalación que acabas de descargar. Dependiendo de tu sistema operativo, podría ser un archivo ejecutable (.exe para Windows) o un archivo comprimido (.zip).
2. Si el archivo está comprimido, descomprímelo en una ubicación de tu elección. En Windows, puedes hacer clic derecho en el archivo y seleccionar "Extraer aquí" para descomprimirlo en la misma carpeta.&#x20;
3. Ejecuta el archivo de instalación. Si estás en Windows, simplemente haz doble clic en el archivo ejecutable (.exe). Si estás en Mac, arrastra el Arduino IDE a la carpeta de Aplicaciones y luego ábrela desde allí.&#x20;
4. Sigue las instrucciones del asistente de instalación. Generalmente, solo necesitarás hacer clic en "Siguiente" o "Continuar" y aceptar los términos de uso.&#x20;
5. Una vez completada la instalación, ¡ya tienes el Arduino IDE listo para ser utilizado en tu ordenador! ¡Felicitaciones!

![](<../../../.gitbook/assets/image (28).png>)



## **Configuración Inicial del Entorno de Desarrollo**

Antes de empezar a programar nuestros proyectos en el Arduino IDE, es importante realizar algunas configuraciones iniciales para asegurarnos de que todo funcione correctamente. A continuación, te explicamos cómo hacerlo:

1. Conecta tu placa Arduino a tu ordenador mediante un cable USB. Asegúrate de que la placa esté correctamente alimentada. \

2.  Abre el Arduino IDE en tu ordenador. Deberías ver una interfaz sencilla con un área de código en blanco. \


    <figure><img src="../../../.gitbook/assets/image (15).png" alt=""><figcaption></figcaption></figure>


3.  Ve al menú "Herramientas" y selecciona el modelo de placa Arduino que estás utilizando. Por ejemplo, si estás utilizando un Arduino UNO, selecciona "Arduino/Genuino UNO" en la lista desplegable. (En la imagen, verás que yo tengo más placas instaladas, por lo que a ti quizás solo te aparezca Arduino AVR Boards y no todas las opciones).\


    <figure><img src="../../../.gitbook/assets/image (8).png" alt=""><figcaption></figcaption></figure>


4.  A continuación, selecciona el puerto serial al que está conectada tu placa Arduino. En Windows, puedes encontrar el puerto en el "Administrador de dispositivos". En Mac, aparecerá como "/dev/cu.usbmodemXXXX" o "/dev/tty.usbmodemXXXX" en la sección de puertos. \


    <figure><img src="../../../.gitbook/assets/image (10).png" alt=""><figcaption></figcaption></figure>


5. ¡Y eso es todo! Tu entorno de desarrollo ya está configurado y listo para empezar a programar.&#x20;

Ahora puedes comenzar a escribir tu primer código, cargarlo en la placa Arduino y ver cómo cobra vida tu proyecto.

Espero que esta guía detallada te haya ayudado a comprender cómo descargar, instalar y configurar el Arduino IDE en tu ordenador. Recuerda que el software Arduino IDE será tu compañero fiel en este emocionante viaje de creación tecnológica. ¡Así que ponte la capa de superhéroe, enciende tu imaginación y prepárate para hacer realidad proyectos asombrosos con Arduino!

No olvides consultar nuestros próximos capítulos, donde exploraremos más conceptos y aprenderemos a utilizar diferentes componentes con el Arduino IDE.

¡Hasta pronto, creadores de POWAR STEAM!
