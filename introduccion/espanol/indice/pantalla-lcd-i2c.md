# Pantalla LCD I2C

¡Bienvenidos al tutorial de conexión de una pantalla LCD al Arduino utilizando la comunicación I2C!

Introducción a las pantallas LCD: Las pantallas LCD (Liquid Crystal Display, por sus siglas en inglés) son dispositivos utilizados para mostrar información en forma de texto, números y gráficos. Estas pantallas son ampliamente utilizadas en proyectos electrónicos debido a su facilidad de uso y su capacidad para mostrar información de manera clara y legible. Existen diferentes tipos de pantallas LCD, que varían en tamaño, resolución y características adicionales.

Una de las opciones populares son las pantallas LCD de 16x2, que consisten en dos filas de dieciséis columnas cada una, lo que permite mostrar hasta 32 caracteres alfanuméricos. Estas pantallas son ideales para mostrar mensajes simples y datos en proyectos de Arduino.

Conexión I2C:&#x20;

La comunicación I2C (Inter-Integrated Circuit, por sus siglas en inglés) es un protocolo de comunicación serial que permite la transmisión de datos entre dispositivos utilizando solo dos cables: el cable de datos (SDA) y el cable de reloj (SCL). El protocolo I2C utiliza una técnica llamada bus multi-maestro, lo que significa que varios dispositivos pueden compartir el mismo bus de comunicación.

En el caso de las pantallas LCD, la conexión I2C se utiliza para simplificar la conexión con Arduino, ya que permite controlar la pantalla utilizando solo dos pines del microcontrolador en lugar de varios pines para la conexión directa. Esto es posible gracias a un módulo I2C integrado en la pantalla LCD, que se encarga de la comunicación con Arduino y proporciona una interfaz fácil de usar.

Proyecto:

Conexión de una pantalla LCD al Arduino utilizando I2C: Ahora que hemos entendido las características de las pantallas LCD y la conexión I2C, podemos proceder a conectar una pantalla LCD al Arduino y enviar un mensaje.

Materiales necesarios:

* Arduino Uno (u otro modelo compatible)
* Pantalla LCD de 16x2 con conexión I2C
* Protoboard
* Cables de conexión

## Conexión de la pantalla LCD al Arduino:

1. Conecta el pin VCC de la pantalla al pin 5V de Arduino.
2. Conecta el pin GND de la pantalla al pin GND de Arduino.
3. Conecta el pin SDA de la pantalla al pin A4 (SDA) de Arduino.
4. Conecta el pin SCL de la pantalla al pin A5 (SCL) de Arduino.

## Instalación de la biblioteca necesaria:

Antes de poder utilizar la pantalla LCD con I2C, debemos instalar una biblioteca en el entorno de desarrollo de Arduino. La biblioteca nos proporcionará funciones y métodos para controlar la pantalla de manera sencilla. Sigue estos pasos para instalar la biblioteca:

1. Abre el IDE de Arduino.
2. Ve a "Sketch" -> "Include Library" -> "Manage Libraries".
3. En el campo de búsqueda, escribe "LiquidCrystal I2C".
4. Aparecerán varias bibliotecas relacionadas con pantallas LCD I2C. Busca "LiquidCrystal I2C" y haz clic en "Install".

Una vez instalada la biblioteca, cierra el administrador de bibliotecas.

## Uso de la pantalla LCD para mostrar un mensaje:

Una vez que hemos realizado la conexión y hemos instalado la biblioteca, podemos utilizar el siguiente código para mostrar el mensaje "Bienvenidos a POWAR STEAM" en la pantalla LCD:

```cpp
cppCopy code#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);  // Dirección de la pantalla y dimensiones (16x2)

void setup() {
  lcd.begin(16, 2);  // Inicializar la pantalla con 16 columnas y 2 filas
  lcd.backlight();  // Encender la retroiluminación de la pantalla
  lcd.setCursor(0, 0);  // Posicionar el cursor en la primera columna de la primera fila
  lcd.print("Bienvenidos a");  // Imprimir el primer mensaje
  lcd.setCursor(0, 1);  // Posicionar el cursor en la primera columna de la segunda fila
  lcd.print("POWAR STEAM");  // Imprimir el segundo mensaje
}

void loop() {
  // El código dentro del bucle loop() se ejecutará continuamente después de la inicialización
}
```

En este ejemplo, se utiliza la biblioteca `Wire` para la comunicación I2C y la biblioteca `LiquidCrystal_I2C` para controlar la pantalla. Se crea un objeto `lcd` con la dirección `0x27` y se configura como una pantalla de 16 columnas y 2 filas. Luego, se enciende la retroiluminación de la pantalla y se coloca el cursor en la posición adecuada para imprimir el mensaje "Bienvenidos a POWAR STEAM" en dos líneas diferentes.

¡Y eso es todo! Ahora tienes los conocimientos necesarios para conectar y utilizar una pantalla LCD de 16x2 con conexión I2C en tus proyectos de Arduino. Puedes experimentar con diferentes mensajes y funciones de la biblioteca para crear visualizaciones personalizadas en la pantalla. ¡Diviértete explorando las capacidades de la pantalla y sigue aprendiendo en el emocionante mundo de Arduino!
