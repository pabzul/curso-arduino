# Señales Digitales y Analogicas

Pines Digitales, Analógicos y PWM

En Arduino, podemos trabajar con señales digitales, analógicas y PWM (Modulación por Ancho de Pulso). Estas señales nos permiten interactuar con diferentes componentes y controlar su funcionamiento de manera precisa. En este artículo, exploraremos las características de cada tipo de señal y aprenderemos a utilizar los pines PWM en Arduino.

Señales Digitales: Las señales digitales son binarias, es decir, tienen dos posibles valores: HIGH o LOW, que corresponden a 1 o 0, encendido o apagado, respectivamente. Estas señales son ideales para enviar y recibir datos binarios o controlar dispositivos que funcionan en base a estados discretos. En Arduino, los pines digitales pueden ser configurados como entradas o salidas digitales.

Cuando un pin digital se configura como entrada, se puede leer el estado de la señal, es decir, si está en HIGH o LOW. Por ejemplo, si conectamos un switch a un pin digital, la señal será LOW (0) cuando el switch esté abierto y HIGH (1) cuando esté cerrado. De manera similar, con un pulsador, la señal será HIGH mientras esté presionado y volverá a LOW cuando se suelte.

Cuando un pin digital se configura como salida, se puede enviar una señal en HIGH o LOW. Esto permite controlar dispositivos como LEDs, relés y motores, encendiéndolos o apagándolos según sea necesario.

Señales Analógicas: A diferencia de las señales digitales, las señales analógicas pueden tener valores en un rango continuo. Los pines analógicos de Arduino tienen una resolución de 10 bits, lo que les permite representar valores de 0 a 1023. Estos pines son útiles para leer y escribir datos analógicos.

Un uso común de los pines analógicos es la lectura de sensores analógicos. Por ejemplo, al conectar un potenciómetro a un pin analógico, podemos obtener valores analógicos en un rango de 0 a 1023, dependiendo de la posición del potenciómetro. Esto nos permite ajustar el brillo de una pantalla, el volumen de un altavoz o cualquier otra aplicación que requiera control continuo.

Además, también podemos escribir señales analógicas en los pines analógicos de salida utilizando la función `analogWrite()`. Sin embargo, es importante mencionar que en Arduino, la generación de señales analógicas mediante `analogWrite()` es en realidad una simulación de señal analógica utilizando PWM.

Pines PWM (Modulación por Ancho de Pulso): Los pines PWM de Arduino generan señales digitales que se asemejan a señales analógicas mediante la variación del ancho del pulso. Esta técnica es utilizada para controlar la intensidad luminosa de los LEDs, la velocidad de los motores y otras aplicaciones que requieren una señal analógica simulada.

En Arduino, los pines PWM están marcados con el símbolo "\~" (tilde) junto a su número de pin en la placa. La resolución de los pines PWM es de 8 bits, lo que significa que pueden generar 256 niveles de intensidad diferentes.

Ejercicio Práctico: Control de Brillo de un LED con PWM

Materiales necesarios:

* Arduino Uno o cualquier placa compatible
* LED
* Resistencia de 220 ohmios
* Cables de conexión

Conexiones:

1. Conecta el ánodo (terminal largo) del LED a un pin digital con soporte de PWM en Arduino (por ejemplo, el pin 9).
2. Conecta el cátodo (terminal corto) del LED a través de la resistencia de 220 ohmios a GND (tierra) de Arduino.

Código:

```cpp
cppCopy codeint ledPin = 9;  // Pin PWM para controlar el LED

void setup() {
  pinMode(ledPin, OUTPUT);  // Configurar el pin como salida
}

void loop() {
  // Controlar el brillo del LED aumentando y disminuyendo el valor de brillo
  for (int brillo = 0; brillo <= 255; brillo++) {
    analogWrite(ledPin, brillo);  // Establecer el brillo del LED
    delay(10);  // Retardo para apreciar el cambio de brillo
  }

  for (int brillo = 255; brillo >= 0; brillo--) {
    analogWrite(ledPin, brillo);  // Establecer el brillo del LED
    delay(10);  // Retardo para apreciar el cambio de brillo
  }
}
```

Explicación: En este ejercicio, controlaremos el brillo de un LED utilizando PWM. El pin 9 de Arduino se configura como salida y se utiliza para controlar el LED. Mediante la función `analogWrite()`, podemos establecer diferentes niveles de brillo del LED.

En el bucle principal `loop()`, utilizamos un bucle `for` para aumentar y disminuir gradualmente el brillo del LED. El valor de brillo va desde 0 hasta 255, y se establece mediante `analogWrite(ledPin, brillo)`. Después de cada cambio de brillo, hay un retardo de 10 milisegundos para que podamos apreciar el cambio gradual.

Al cargar y ejecutar este programa en Arduino, el LED comenzará a cambiar su brillo de forma suave y continua, creando un efecto de atenuación.

Conclusión: En este artículo, hemos explorado los conceptos de señales digitales, analógicas y PWM en Arduino. Hemos aprendido cómo utilizar los pines digitales y analógicos para interactuar con dispositivos y sensores, y cómo utilizar los pines PWM para generar señales analógicas simuladas.

El uso de señales digitales, analógicas y PWM amplía las capacidades de Arduino, permitiéndonos crear proyectos más complejos y precisos. Ahora, con este conocimiento, puedes comenzar a experimentar y crear tus propios proyectos utilizando diferentes tipos de señales. ¡Diviértete explorando el mundo de Arduino y descubriendo todas las posibilidades que ofrece!
