# 12 - Motor DC

Título: Dominando el Motor DC: Control y Proyectos con Arduino

Introducción: ¡Saludos a todos los entusiastas de POWAR STEAM! En esta entrada de blog, nos adentraremos en el fascinante mundo de los motores DC y aprenderemos a utilizarlos como actuadores en nuestros proyectos con Arduino. Exploraremos cómo controlar los motores utilizando diferentes componentes, como potenciómetros, sensores, botones y más. Prepárate para explorar el emocionante mundo de la electrónica y la programación.

¿Qué es un motor DC? Un motor DC (corriente continua) es un dispositivo electromecánico que convierte la energía eléctrica en movimiento mecánico. Vienen en diferentes tamaños y potencias, y funcionan mediante la interacción de imanes y corriente eléctrica para impulsar el movimiento del eje del motor.

Otros tipos de motores: Además de los motores DC, existen otros tipos comunes de motores utilizados en proyectos de electrónica y robótica, como servos y stepper motors. Los servos son motores controlados por posición, ideales para movimientos precisos, mientras que los stepper motors se mueven en incrementos discretos y predefinidos, perfectos para proyectos que requieren movimientos precisos y repetitivos.

##

## **Lista de Materiales:**&#x20;

Para llevar a cabo los proyectos con el sensor de luz, necesitarás los siguientes materiales:

<table><thead><tr><th width="254">COMPONENTE:</th><th width="372.3333333333333">IMAGEN:</th><th align="center">CANTIDAD:</th></tr></thead><tbody><tr><td>Arduino Uno</td><td><img src="../../../.gitbook/assets/ARDUINO UNO (2).jpg" alt="" data-size="original"></td><td align="center">1</td></tr><tr><td>Cable Arduino USB</td><td><img src="../../../.gitbook/assets/USB wire arduino (1).jpg" alt=""></td><td align="center">1</td></tr><tr><td>Motor DC</td><td><img src="../../../.gitbook/assets/DC motor.jpg" alt=""></td><td align="center">1</td></tr><tr><td>Potenciómetro</td><td><img src="../../../.gitbook/assets/potenciometro.jpg" alt=""></td><td align="center">1</td></tr><tr><td>Protoboard</td><td><img src="../../../.gitbook/assets/Protoboard.jpg" alt=""></td><td align="center">1</td></tr><tr><td>Cables de conexión</td><td><img src="../../../.gitbook/assets/jumperwires.png" alt=""></td><td align="center">1</td></tr></tbody></table>



## Control de Velocidad del Motor DC mediante Código:

&#x20;En este proyecto, aprenderemos a controlar la velocidad del motor DC utilizando el código en Arduino.

### Conexiones:&#x20;

Comencemos conectando los componentes de la siguiente manera:

<figure><img src="../../../.gitbook/assets/14 DC Motor_bb.png" alt=""><figcaption></figcaption></figure>

* Conecta el pin positivo (+) del motor DC al pin digital 9 del Arduino utilizando un cable jumper.
* Conecta el pin negativo (-) del motor DC al pin GND (tierra) del Arduino utilizando otro cable jumper.

###

### Programación:&#x20;

Abre el IDE de Arduino y utiliza el siguiente código para controlar la velocidad del motor DC:

```cpp
cppCopy code// Definición de pines
const int motorPin = 9;

void setup() {
  // Configuración del pin como salida
  pinMode(motorPin, OUTPUT);
}

void loop() {
  // Cambiar la velocidad del motor
  analogWrite(motorPin, 128); // Prueba diferentes valores entre 0 y 255
  delay(1000); // Ajusta el tiempo de espera según tus preferencias
  
  // Cambiar la velocidad del motor
  analogWrite(motorPin, 50); // Prueba diferentes valores entre 0 y 255
  delay(500); // Ajusta el tiempo de espera según tus preferencias
}
```

Este código utiliza la función "analogWrite()" para establecer la velocidad del motor DC en valores entre 0 y 255. Puedes experimentar con diferentes valores para ajustar la velocidad según tus preferencias. Hemos agregado un segundo cambio de velocidad para mostrar cómo combinar diferentes valores y tiempos de espera para lograr efectos interesantes.

Control Creativo de Motores DC: Además de los potenciómetros, existen numerosos componentes y dispositivos que se pueden utilizar para controlar los motores DC, como sensores, botones, controladores de motor, joysticks, entre otros. La elección del componente dependerá de los requisitos específicos de tu proyecto y de las funcionalidades que desees implementar.



## Proyecto 2: Ventilador Activado por Potenciómetro

En este proyecto, utilizaremos un potenciómetro para controlar la velocidad del motor DC. Un potenciómetro es un componente electrónico que permite controlar la resistencia eléctrica de manera variable. Consiste en una resistencia ajustable con un cursor móvil que se desplaza a lo largo de la resistencia. Al girar el eje del potenciómetro, se cambia la posición del cursor y, por lo tanto, la resistencia total entre los terminales del componente. Esto permite regular la cantidad de corriente que fluye a través del potenciómetro y controlar, por ejemplo, el volumen de un altavoz o el brillo de una pantalla. Los potenciómetros son ampliamente utilizados en electrónica y son una herramienta útil para ajustar parámetros en diferentes aplicaciones.



### Conexiones:

Antes de comenzar, realiza las siguientes conexiones:

<figure><img src="../../../.gitbook/assets/15 DC Motor+potenciometro_bb.png" alt=""><figcaption></figcaption></figure>

* Conecta uno de los pines del potenciómetro al pin analógico A0 del Arduino.
* Conecta el pin central (wiper) del potenciómetro a una línea de alimentación de 5V en el protoboard.
* Conecta el pin positivo (+) del motor DC al pin digital 9 del Arduino.
* Conecta el pin negativo (-) del motor DC al pin GND (tierra) del Arduino.

###

### Programación:&#x20;

Ahora, vamos a utilizar el siguiente código para leer los valores del potenciómetro y controlar la velocidad del motor en función de esos valores:

```cpp
cppCopy code// Definición de pines
const int motorPin = 9;
const int potPin = A0;

void setup() {
  // Configuración de los pines
  pinMode(motorPin, OUTPUT);
}

void loop() {
  // Leer el valor del potenciómetro
  int potValue = analogRead(potPin);

  // Mapear el valor del potenciómetro al rango de velocidad del motor
  int speed = map(potValue, 0, 1023, 0, 255);

  // Establecer la velocidad del motor
  analogWrite(motorPin, speed);
}
```

Este código lee los valores analógicos del potenciómetro utilizando la función "analogRead()" y luego utiliza la función "map()" para ajustar esos valores al rango de velocidad del motor (de 0 a 255). Finalmente, establece la velocidad del motor utilizando la función "analogWrite()" con el valor mapeado.



En este artículo, hemos explorado el emocionante mundo de los motores DC y aprendido cómo controlar su velocidad utilizando Arduino. Hemos comprendido qué es un motor DC, sus diferentes tamaños y potencias, y hemos mencionado otros tipos de motores como servos y stepper motors. Además, hemos descubierto que los motores pueden ser controlados utilizando una variedad de componentes, como potenciómetros, sensores, botones y más. Te animamos a que experimentes con estos proyectos y explores formas creativas de utilizar los motores DC en tus propios proyectos.

¡Diviértete y mantén la creatividad en marcha!
