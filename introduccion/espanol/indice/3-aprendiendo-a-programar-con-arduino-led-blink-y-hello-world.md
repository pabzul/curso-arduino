---
description: Programación básica con Arduino IDE
---

# 3 - Aprendiendo a programar con Arduino: LED Blink y Hello World

¡Hola a todos los entusiastas de Arduino! En nuestro emocionante viaje de descubrimiento de Arduino, hemos llegado al capítulo 2, donde aprenderemos sobre la programación con Arduino. En este capítulo, aprenderemos los conceptos básicos de programación con Arduino utilizando el software Arduino IDE. Exploraremos la estructura de un programa Arduino y te guiaremos a través de dos ejemplos prácticos: uno para encender y apagar un LED y otro para enviar mensajes al monitor serial.



## **Lista de componentes necesarios:**&#x20;

Antes de comenzar, asegurémonos de tener los siguientes componentes a mano para nuestros proyectos:

<table><thead><tr><th width="254">COMPONENTE:</th><th width="372.3333333333333">IMAGEN:</th><th align="center">CANTIDAD:</th></tr></thead><tbody><tr><td>Arduino Uno</td><td><img src="../../../.gitbook/assets/ARDUINO UNO (2).jpg" alt="" data-size="original"></td><td align="center">1</td></tr><tr><td>Cable Arduino USB</td><td><img src="../../../.gitbook/assets/USB wire arduino (1).jpg" alt=""></td><td align="center">1</td></tr></tbody></table>



## Estructura básica de un programa Arduino

<figure><img src="../../../.gitbook/assets/image (51).png" alt=""><figcaption></figcaption></figure>

Antes de comenzar a programar, es importante comprender la estructura básica de un programa Arduino. Un programa Arduino consta de dos partes principales: la función "**setup**" y la función "**loop**".

**La función "setup"** se ejecuta una vez al inicio del programa y se utiliza para realizar configuraciones iniciales. Aquí es donde podemos establecer los pines de entrada o salida y realizar otras configuraciones necesarias.

**La función "loop"** se ejecuta continuamente y es donde escribiremos el código que queremos que se repita una y otra vez. Dentro de la función "loop", podemos realizar acciones como encender o apagar LEDs, leer datos de sensores, controlar motores y mucho más.



## Proyecto 1: LED Blink (Encender y apagar un LED)

<figure><img src="../../../.gitbook/assets/image (19).png" alt=""><figcaption></figcaption></figure>

En este tutorial, aprenderemos a encender y apagar un LED utilizando el pin LED\_BUILTIN de Arduino. El pin LED\_BUILTIN está asociado al LED incorporado en la placa Arduino y su número de pin puede variar dependiendo del modelo de placa que estés utilizando. En la mayoría de las placas, el pin LED\_BUILTIN está conectado al pin 13.

A continuación, te mostraremos cómo realizar este tutorial sin conectar un LED externo. Sin embargo, más adelante en otro capítulo, podrás probar este mismo código conectando un LED externo para obtener resultados visuales.

###

### Paso 1: Configuración del pin LED\_BUILTIN:

* Abre el software Arduino IDE.
* Conecta tu Arduino al ordenador utilizando el cable USB.
* Selecciona el tipo de placa y el puerto correcto en el menú "Herramientas".
* Escribe el siguiente código en el entorno de desarrollo de Arduino:

```cpp
void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Configurar el pin LED_BUILTIN como salida
}
```

* En la función "setup", utilizamos la instrucción pinMode para configurar el pin LED\_BUILTIN como una salida. Al establecerlo como salida, permitimos que el pin controle el estado del LED.

{% hint style="info" %}
_En el código de Arduino, los comentarios explicativos que comienzan con "//" son útiles para añadir notas y aclaraciones dentro del código, pero no afectan su funcionamiento._
{% endhint %}



### Paso 2: Encender y apagar el LED

* Escribe el siguiente código después de la función "setup":

```cpp
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}
```

* Dentro de la función "loop", utilizamos la instrucción `digitalWrite` para establecer el estado del pin del LED en "HIGH" (encendido) y "LOW" (apagado). Luego, utilizamos la función `delay` para pausar el programa durante un segundo antes de cambiar el estado del LED.

```arduino
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}
```



### Paso 3: Subir el programa:

* Haz clic en el botón que tiene una flecha en la esquina superior izquierda. Este botón revisará primero que todo tu código este correcto, y luego cargará el código en tu Arduino.

{% hint style="info" %}
El botón con el "✔" que está junto a la flecha solo "compilará" el código para ver que todo esté bien, sin subirlo. Compilar significa traducir este código que tú has escrito en un código que comprenda el arduino. Es algo como traducir.
{% endhint %}

<figure><img src="../../../.gitbook/assets/image (25).png" alt=""><figcaption></figcaption></figure>

### Paso 4: Modificar el parpadeo del LED:

* No es necesario conectar un LED externo, ya que utilizaremos el LED incorporado en la placa Arduino.
* Enciende el Arduino y verás cómo el LED incorporado parpadea, alternando entre encendido y apagado con un intervalo de 1 segundo.

¡Felicidades! Ahora sabes cómo programar Arduino utilizando el software Arduino IDE. Has aprendido la estructura básica de un programa Arduino y has creado un programa que enciende y apaga un LED. Estos son los primeros pasos en el emocionante mundo de la programación con Arduino. Este es uno de los ejemplos más usados para aprender Arduino y se le llama LED BLINK (parpadeo de LED).



## Introducción a la Comunicación Serial

La comunicación serial es una forma de transmitir datos bit a bit entre un Arduino y un dispositivo externo, como un ordenador. Se utiliza ampliamente para depurar, monitorear y enviar datos desde y hacia Arduino. Uno de los principales beneficios de la comunicación serial es la capacidad de enviar información en tiempo real y recibir respuestas desde el monitor serial.

En Arduino, utilizamos el monitor serial para visualizar mensajes, depurar programas y comunicarnos con el Arduino durante la ejecución del código. El monitor serial es una ventana de texto en el entorno de desarrollo de Arduino que muestra los mensajes enviados por el programa.



## Proyecto 2: Enviando un Mensaje al Monitor Serial

En este tutorial, aprenderemos a enviar un mensaje simple al monitor serial utilizando Arduino. Sigue los siguientes pasos:



### Conexiónes:

* Conecta tu Arduino al ordenador utilizando el cable USB.
* Abre el software Arduino IDE.
* Selecciona el tipo de placa y el puerto correcto en el menú "Herramientas".



### Código:

*   Escribe el siguiente código en el entorno de desarrollo de Arduino:

    ```cpp
    cppCopy codevoid setup() {
      Serial.begin(9600);  // Iniciar la comunicación serial con una velocidad de 9600 baudios
    }

    void loop() {
      Serial.println("Hola Pablo");
      Serial.println("Bienvenido a POWAR");
      delay(1000);
    }
    ```
* En este tutorial, utilizamos la función Serial.begin() para iniciar la comunicación serial con una velocidad de baudios de 9600. Luego, en el bucle loop(), utilizamos la función Serial.println() para enviar los mensajes "Hola Pablo" y "Bienvenido a POWAR" al monitor serial. El comando delay(1000) pausa la ejecución del programa durante 1 segundo antes de repetir el envío de los mensajes.
* Haz clic en el botón de subida de programa para cargar el código en tu Arduino.



### Observar el monitor serial:

* Haz clic en el ícono de la lupa en la esquina superior derecha del entorno de desarrollo de Arduino para abrir el monitor serial.
*

    <figure><img src="../../../.gitbook/assets/image (64).png" alt=""><figcaption></figcaption></figure>
* Asegúrate de que la velocidad de baudios en el monitor serial coincida con la velocidad de baudios especificada en tu programa (9600 en este caso).
* Verás que se imprime el mensaje "Hola Pablo" y "Bienvenido a POWAR" repetidamente con un intervalo de 1 segundo.



Puedes experimentar cambiando los textos y ajustando los tiempos en el código para explorar diferentes mensajes y comportamientos. También podrías modificar el primer tutorial incluso, para que en el monitor serial te dijera el estado del LED (HIGH o LOW).

Este tutorial es una introducción a la comunicación serial y su relación con el clásico programa "Hello World". Ahora puedes utilizar esta técnica para imprimir mensajes de depuración, monitorear variables y comunicarte con Arduino durante la ejecución de tus programas.

En los próximos capítulos, exploraremos más conceptos y realizaremos proyectos más emocionantes en los que usarás también estos conocimientos.

¡No te lo pierdas!
