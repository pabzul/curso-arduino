---
description: Aprendiendo a programar con Arduino
---

# Guión 3

\[Intro musical animada]

\[Créditos de introducción]

(Pablo, sonriente y entusiasta, se dirige a la cámara)

Pablo: ¡Hola a todos los amantes de la tecnología! Soy Pablo, su amigo entusiasta de POWAR STEAM, y hoy continuaremos nuestro emocionante tutorial de Arduino. En este capítulo, vamos a aprender a programar con Arduino y dar vida a nuestras creaciones. ¡Empecemos!

\[Pablo muestra la página web de POWAR STEAM en la pantalla]

Pablo: Pero antes de comenzar, quiero recordarles que todos estos video tutoriales tienen una página web complementaria en la que encontrarán los códigos y esquemas de conexión que aparecen en los video tutoriales. Pueden visitar nuestra página en [www.arduino.powarsteam.com](http://www.arduino.powarsteam.com/) para acceder a todos estos recursos adicionales.

\[Pablo hace un gesto animado de "ven" con su mano]

Pablo: Bien, ahora vamos a sumergirnos en el mundo de la programación con Arduino. Como mencionamos en el capítulo anterior, POWAR STEAM es una compañía que desarrolla herramientas y metodologías STEAM, Arduino, impresión 3D y fabricación digital. Además, ofrecemos un Kit de Introducción a la Robótica que contiene todos los componentes necesarios para este curso de Arduino.

\[Pablo muestra el Kit de Introducción a la Robótica de POWAR STEAM]

Pablo: Recuerden, si desean obtener todos los componentes necesarios, les invito a visitar nuestro Kit de Introducción a la Robótica en nuestra página web. Ahí encontrarán todo lo que necesitan para seguir este curso completo de Arduino.

\[Pablo hace un gesto animado de "kit" con sus manos]

Pablo: Ahora, vamos a sumergirnos en la programación de Arduino. Como siempre, les doy la bienvenida a nuestro canal y les invito a suscribirse para no perderse ninguno de nuestros emocionantes capítulos.

\[Pablo hace un gesto de "bienvenida" con sus manos]

Pablo: Antes de empezar a escribir código, es importante comprender la estructura básica de un programa Arduino. Un programa Arduino consta de dos partes principales: la función "setup" y la función "loop". La función "setup" se ejecuta una vez al inicio del programa y se utiliza para realizar configuraciones iniciales, como la configuración de pines y la inicialización de variables. La función "loop" se ejecuta continuamente y es donde escribimos el código que queremos que se repita una y otra vez.

\[Pablo muestra la estructura básica de un programa Arduino en la pantalla]

Pablo: Ahora que conocemos la estructura básica de un programa Arduino, vamos a escribir nuestro primer programa sencillo. Imaginemos que queremos encender un LED conectado a nuestro Arduino. ¡Vamos a hacerlo juntos!

\[Pablo muestra un esquema de un Arduino con un LED conectado y un código de ejemplo]

Pablo: Paso número uno: dentro de la función "setup", vamos a indicar que el pin al que está conectado nuestro LED es de salida. De esta forma, le diremos a Arduino que debe enviar una señal para encenderlo.

\[Pablo muestra el código de ejemplo y resalta la línea para configurar el pin de salida]

Pablo: Paso número dos: dentro de la función "loop", vamos a escribir el código para encender y apagar el LED. Usaremos las funciones "digitalWrite" y "delay" para controlar el estado del LED y agregar un pequeño retraso entre encendido y apagado.

\[Pablo muestra el código de ejemplo y resalta las líneas para encender y apagar el LED]

Pablo: ¡Y eso es todo! Hemos escrito nuestro primer programa Arduino para encender un LED. Ahora solo nos queda cargar este programa en nuestro Arduino y ver cómo se ilumina el LED.

\[Pablo muestra el resumen de los pasos y se enfoca en el Arduino conectado al ordenador]

Pablo: En el próximo video, profundizaremos en el lenguaje de programación Arduino C y exploraremos diferentes acciones que podemos realizar con nuestros Arduino. ¡Será emocionante!

\[Pablo muestra un adelanto de los temas que se verán en el próximo video]

Pablo: Eso es todo por hoy, amigos y amigas. Espero que hayan disfrutado este capítulo sobre la programación básica con Arduino. Recuerden que POWAR STEAM ofrece el Kit de Introducción a la Robótica en nuestra página de internet, donde podrán encontrar todos los componentes necesarios para este curso de Arduino. Además, si desean profundizar sus conocimientos, les recomendamos visitar nuestro curso completo en [www.arduino.powarsteam.com](http://www.arduino.powarsteam.com/). Allí encontrarán más tutoriales, proyectos y recursos para seguir aprendiendo.

\[Pablo hace un gesto animado de despedida]

Pablo: ¡Gracias por acompañarnos en esta emocionante aventura en el mundo de Arduino! ¡Nos vemos en el próximo video!

\[Outro musical animado]
