---
description: Uso de un pulsador como entrada
---

# Guión 8

**Presentador:** ¡Hola a todos y bienvenidos de nuevo a nuestro canal de tutoriales de Arduino! En el tutorial de hoy, vamos a aprender cómo utilizar un pulsador como entrada en nuestros proyectos. Aprenderemos cómo conectar y utilizar un pulsador, cómo leer su estado en Arduino y cómo controlar un LED mediante el pulsador. ¡Así que sin más preámbulos, vamos a comenzar!

\[Pantalla muestra el título del capítulo]

Empecemos por entender qué es un pulsador y cómo podemos utilizarlo como entrada en nuestros proyectos.\
\
\[Imagen muestra un pulsador y el símbolo de entrada digital en un esquema]\
\
Un pulsador es similar a un interruptor, pero en lugar de tener una posición fija, se presiona para cerrar el circuito y se suelta para abrirlo. Esto nos permite controlar el flujo de corriente en nuestro proyecto. En Arduino, utilizaremos los pines digitales para leer el estado del pulsador, es decir, si está presionado o no.\
\
\[Imagen muestra los componentes dispuestos en la mesa]

Antes de comenzar, asegurémonos de tener los siguientes componentes a mano: una placa Arduino Uno, un pulsador, una resistencia de 10kΩ, un LED y una protoboard. También necesitaremos cables de conexión.&#x20;

\[Imagen muestra un esquema de conexión con el pulsador, la resistencia y los cables]

La conexión del pulsador a Arduino es bastante sencilla. Utilizaremos una resistencia de 10kΩ para garantizar un buen funcionamiento del circuito. Asegúrate de seguir los pasos correctos para una conexión adecuada.&#x20;

Vamos a conectar una pata del pulsador a 5V en Arduino. \[Imagen muestra la conexión entre la pata del pulsador y el pin 5V en Arduino] Luego, conectaremos la otra pata del pulsador a uno de los pines digitales en Arduino, por ejemplo, el pin 2. \[Imagen muestra la conexión entre la pata del pulsador y el pin digital 2 en Arduino] Conectaremos una pata de la resistencia de 10kΩ al mismo pin digital donde conectamos el pulsador. \[Imagen muestra la conexión de la resistencia entre el pin digital 2 y el pulsador] Finalmente, conectaremos la otra pata de la resistencia al GND (tierra) en Arduino. \[Imagen muestra la conexión de la resistencia al GND en Arduino]

\[Imagen muestra la apertura del Arduino IDE y la creación de un nuevo sketch]

Ahora que hemos realizado la conexión física, es hora de escribir el código para leer el estado del pulsador en Arduino. Abriremos el Arduino IDE y crearemos un nuevo sketch.&#x20;

En nuestro código, utilizaremos el pin digital 2 como entrada y el pin digital 13 para controlar el LED. \[Imagen muestra el código con las definiciones de pines] Primero, configuraremos el pin 2 como entrada con la función `pinMode()`. Luego, en el bucle principal, utilizaremos la función `digitalRead()` para leer el estado del pulsador. Si el pulsador está presionado, encenderemos el LED utilizando la función `digitalWrite()`, y si no está presionado, apagaremos el LED.&#x20;

\[Imagen muestra el código con las instrucciones para leer el estado del pulsador y controlar el LED]

**Presentador:** Una vez que hayamos escrito nuestro código, carguemos el programa en la placa Arduino y veamos cómo funciona. \[Imagen muestra la carga del programa en la placa Arduino]

**Presentador:** ¡Y voilà! Ahora puedes presionar el pulsador y ver cómo el LED se enciende y se apaga según su estado. ¡Es asombroso! Puedes experimentar modificando el código y agregando más funcionalidades a tu proyecto.

**Presentador:** En resumen, en este tutorial hemos aprendido cómo utilizar un pulsador como entrada en Arduino. Hemos conectado el pulsador, leído su estado y controlado un LED mediante el pulsador. Además, hemos explorado cómo escribir el código necesario para lograrlo.

**Presentador:** Recuerda que la práctica es clave para dominar estos conceptos. ¡No dudes en experimentar y crear tus propios proyectos utilizando pulsadores! Si tienes alguna pregunta, déjala en la sección de comentarios y estaré encantado de ayudarte.

**Presentador:** Esto es todo por hoy. Espero que hayas disfrutado de este tutorial y que te haya resultado útil. Nos vemos en el próximo video, donde continuaremos explorando el fascinante mundo de Arduino. ¡Hasta luego!

**Cierre del video:** ¡Y eso es todo para este capítulo! Espero que hayan disfrutado del tutorial sobre el uso de un pulsador como entrada en Arduino. Recuerden que la electrónica y la programación son campos emocionantes que ofrecen infinitas posibilidades. Si tienen alguna pregunta, no duden en dejarla en los comentarios. ¡Nos vemos en el próximo tutorial, donde continuaremos explorando el apasionante mundo de Arduino!
