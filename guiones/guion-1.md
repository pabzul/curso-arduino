# Guión 1

\[Intro musical animado]

\[Créditos de introducción]

(Pablo, sonriente y emocionado, se dirige a la cámara)

Pablo: ¡Hola a todos los creadores de POWAR STEAM! ¡Soy Pablo, su amigo entusiasta de la tecnología, y hoy vamos a sumergirnos en un mundo emocionante: el mundo de Arduino! ¿Están listos para descubrir qué es Arduino y por qué es tan genial? ¡Pues vamos a ello!

\[Pablo señala una placa Arduino que sostiene en sus manos]

Pablo: Miren esto, chicos y chicas. Esto es Arduino. Es una pequeña placa, pero muy poderosa. En su interior, tiene un microcontrolador, que es como el cerebro de Arduino. ¿Saben qué es un microcontrolador? ¡Es como tener un miniordenador capaz de recibir y enviar señales para hacer que las cosas sucedan!

\[Imágenes animadas de un cerebro y un microcontrolador se superponen para ilustrar la analogía]

Pablo: Imaginen a Arduino como un superhéroe tecnológico. Es ese amigo inteligente que nos ayuda a dar vida a nuestras ideas y proyectos electrónicos. Con Arduino, podemos hacer que las luces se enciendan, los motores funcionen, los sensores detecten el entorno y mucho más. ¡Es como tener un superpoder en nuestras manos!

\[Pablo se coloca una capa imaginaria y hace un gesto emocionado]

Pablo: Pero, ¿qué hace que Arduino sea tan especial? Bueno, es su versatilidad. Podemos conectar todo tipo de componentes a Arduino, como luces, sensores, motores e incluso hacer que interactúen entre ellos. ¡Es como construir nuestro propio mundo de magia tecnológica!

\[Imágenes animadas de diferentes componentes electrónicos se muestran en la pantalla]

Pablo: Ahora que sabemos qué es Arduino y por qué es tan genial, ¡estoy emocionado por comenzar esta aventura juntos! En los próximos capítulos, exploraremos más conceptos emocionantes y aprenderemos cómo utilizar diferentes componentes con Arduino. ¡Así que no se lo pierdan!

\[Pablo hace un gesto animado de "hacia adelante" con su mano]

Pablo: Pero antes de irnos, quiero presentarles a algunos amigos de Arduino: los otros microcontroladores. Además de Arduino, existen otros dispositivos increíbles que también podemos utilizar en nuestros proyectos. ¡Vamos a echarles un vistazo!

\[Pablo hace un gesto de "vamos" con su mano y la pantalla muestra imágenes de otros microcontroladores como micro:bit, ESP32, ESP8266 y Raspberry Pi]

Pablo: Estos son solo algunos de los microcontroladores que podemos utilizar. Cada uno tiene sus propias características y ventajas, ¡pero todos comparten la emoción de la creación tecnológica!

\[Pablo se acerca a una pizarra o pantalla donde aparecen imágenes de los diferentes fabricantes de placas Arduino]

Pablo: Ahora, hablemos de los fabricantes de las placas Arduino. Arduino es una tecnología de código abierto, lo que significa que su diseño y especificaciones están disponibles para que cualquiera pueda usarlos y modificarlos. Esto ha permitido que diferentes fabricantes produzcan sus propias versiones de placas Arduino.

\[Imágenes de diferentes fabricantes de placas Arduino se muestran en la pantalla]

Pablo: Tenemos a Arduino LLC, la organización original que inició este increíble proyecto. También hay otros fabricantes como Adafruit, SparkFun y Elegoo, entre otros. Cada uno de ellos ofrece sus propias variantes de placas Arduino, pero todas funcionan de manera similar y se basan en el mismo principio de programación y conexión de componentes.

\[Pablo señala las imágenes de los fabricantes en la pantalla]

Pablo: Además, esta comunidad de fabricantes y usuarios ha creado una comunidad global de makers apasionados por la tecnología. Podemos compartir nuestros proyectos, colaborar, aprender de los demás y encontrar una gran cantidad de recursos en línea. ¡Es una comunidad emocionante a la que todos podemos unirnos!

\[Pablo hace un gesto animado de "ven" con su mano]

Pablo: Ahora que sabemos qué es Arduino, qué es un microcontrolador y quiénes son los fabricantes de las placas Arduino, estamos listos para comenzar nuestra increíble aventura en el mundo de Arduino. En los próximos capítulos, aprenderemos cómo conectar componentes, programar y crear proyectos emocionantes.

\[Pablo hace un gesto de "hacia adelante" con su mano]

Pablo: ¡Así que no se pierdan ni un solo episodio de esta serie de tutoriales de Arduino en POWAR STEAM! ¡Vamos a divertirnos, aprender y crear juntos!

\[Pablo se despide con entusiasmo]

Pablo: Eso es todo por hoy, amigos y amigas de POWAR STEAM. Espero que les haya gustado esta introducción a Arduino y los microcontroladores. Recuerden, ¡la creatividad y la diversión no tienen límites cuando se trata de Arduino! ¡Nos vemos en el próximo video!

\[Outro musical animado]
