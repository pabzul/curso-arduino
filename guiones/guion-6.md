---
description: Introducción a la Protoboard y sus Componentes
---

# Guión 6

**Presentador (sonriendo y entusiasta):** ¡Hola a todos los amantes de la electrónica y bienvenidos de nuevo a POWAR STEAM! En este emocionante capítulo, vamos a adentrarnos en el mundo de la protoboard y aprender cómo utilizarla para conectar nuestros componentes electrónicos. ¿Están listos? ¡Pues comencemos!

**\[Escena 1: Explicación de la Protoboard y sus Zonas]**

**Presentador (sosteniendo una protoboard):** Aquí tenemos una protoboard, una herramienta fantástica que nos permite realizar conexiones eléctricas de manera rápida y segura. La protoboard se compone de dos zonas principales: la zona de conexiones y la zona de alimentación.

**\[Imágenes animadas muestran la protoboard y sus zonas]**

**Presentador:** La zona de conexiones es donde colocamos y conectamos nuestros componentes electrónicos. Cada fila horizontal en esta zona está conectada internamente, lo que significa que si colocamos un componente en la misma fila, se establecerá automáticamente una conexión eléctrica entre ellos.

La zona de alimentación se encuentra a lo largo de los bordes de la protoboard. Aquí encontramos líneas de alimentación positiva (+) y negativa (-) que nos permiten suministrar energía a nuestros componentes.

**\[Escena 2: Demostración de la Colocación de Componentes]**

**Presentador (muestra componentes y los coloca en la protoboard):** Ahora que conocemos las zonas de la protoboard, vamos a ver cómo colocar nuestros componentes en ella. Aquí tengo algunos componentes electrónicos como resistencias, LED y un sensor de temperatura.

**\[Imágenes en pantalla muestran la colocación de componentes en la protoboard]**

**Presentador:** Observen cómo al colocar los componentes en la misma fila, se establece automáticamente una conexión eléctrica entre ellos. Esto nos permite crear circuitos de manera rápida y sencilla.

**\[Escena 3: Uso de Cables de Puente para Realizar Conexiones]**

**Presentador (sosteniendo cables de puente):** Ahora que hemos colocado nuestros componentes, necesitamos establecer conexiones entre ellos. Para ello, utilizaremos cables de puente o jumpers.

**\[Imágenes en pantalla muestran el uso de cables de puente para realizar conexiones]**

**Presentador:** Con los cables de puente, simplemente los conectamos entre los puntos de la protoboard donde queremos establecer una conexión. Esto nos brinda flexibilidad para diseñar nuestros circuitos de manera personalizada.

**\[Escena 4: Explicación del Uso de las Líneas de Alimentación]**

**Presentador (señalando las líneas de alimentación):** Las líneas de alimentación en los bordes de la protoboard nos permiten suministrar energía a nuestros componentes. La línea de alimentación positiva (+) se conecta a la fuente de alimentación positiva, mientras que la línea de alimentación negativa (-) se conecta a la fuente de alimentación negativa o tierra.

**\[Imágenes en pantalla muestran la conexión de componentes a las líneas de alimentación]**

**Presentador:** Es importante recordar conectar correctamente los componentes a las líneas de alimentación para asegurarnos de que funcionen correctamente.

**\[Escena 5: Cierre y Despedida del Presentador]**

**Presentador (entusiasmado):** ¡Y eso es todo por hoy, amigos y amigas de POWAR STEAM! Ahora que hemos aprendido cómo utilizar la protoboard para conectar componentes y establecer conexiones, estaremos preparados para realizar emocionantes proyectos electrónicos en futuros capítulos.

Recuerden explorar, experimentar y dar rienda suelta a su creatividad. ¡Nos vemos en el próximo video!

**\[Música de cierre animada]**
