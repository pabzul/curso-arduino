# Look and feel y estructura

### Escenario y presentador:

El video tutorial se desarrolla en un set de grabación colorido y dinámico, con estanterías llenas de componentes electrónicos y una pantalla grande donde se proyectan imágenes y diagramas relacionados con Arduino. Al inicio del video, vemos a Pablo, el presentador carismático y entusiasta de POWAR STEAM, vestido con una camiseta con el logotipo de Arduino y sosteniendo una placa Arduino en sus manos.

### Estructura general:

El video tutorial tiene una duración aproximada de 10 minutos. Se divide en varias secciones, comenzando con una introducción emocionante sobre Arduino y sus aplicaciones. Luego se presenta la información sobre los diferentes microcontroladores, se destaca la historia de Arduino y se explica de forma sencilla qué es un microcontrolador. Se utilizan analogías divertidas para ayudar a comprender los conceptos. Durante el video, se muestran imágenes y gráficos relacionados con Arduino y los microcontroladores mencionados.
