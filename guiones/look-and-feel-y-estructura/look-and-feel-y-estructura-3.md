# Look & feel y estructura

**Escenario:** El presentador está frente a una mesa donde se encuentra una placa Arduino Uno, un pulsador, una resistencia de 10kΩ, un LED y una protoboard. Hay cables de conexión a mano.

**Estructura General:** El tutorial se dividirá en los siguientes pasos:

1. Introducción y presentación del tema.
2. Explicación del concepto de un pulsador y su uso como entrada digital.
3. Lista de componentes necesarios para realizar los ejercicios.
4. Explicación detallada sobre la conexión del pulsador a Arduino.
5. Paso a paso: Conexión del pulsador y explicación de cada conexión.
6. Explicación detallada de cómo leer el estado del pulsador en Arduino.
7. Paso a paso: Lectura del estado del pulsador y reacción mediante un LED.
8. Cierre y conclusiones.
