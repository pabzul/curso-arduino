# Look and feel y estructura

### Escenario:&#x20;

El presentador, llamado Laura, se encuentra frente a una pantalla verde con un ordenador y una placa Arduino colocados en una mesa. En la pantalla verde se mostrarán imágenes, texto y otros elementos visuales para ilustrar los pasos y conceptos mencionados en el tutorial.

### Presentador:&#x20;

Laura, una joven entusiasta de la tecnología con habilidades de comunicación claras y amigables. Ella explica los conceptos de manera sencilla y utiliza analogías para ayudar a los espectadores a comprender mejor.

### Estructura general:

1. Introducción: Laura se presenta y da la bienvenida a los espectadores. Explica que en este video tutorial aprenderán cómo descargar e instalar el software Arduino IDE, así como realizar la configuración inicial del entorno de desarrollo.
2. Descarga del Arduino IDE: Laura explica los pasos para descargar el Arduino IDE, mientras se muestran imágenes de la página oficial de Arduino y se resalta el enlace de descarga correspondiente al sistema operativo del espectador.
3. Instalación del Arduino IDE: Laura muestra cómo abrir el archivo de instalación descargado y explica los pasos para completar la instalación en diferentes sistemas operativos. Se muestran imágenes de los pasos mencionados, como hacer clic en "Siguiente" y aceptar los términos de uso.
4. Configuración inicial del entorno de desarrollo: Laura conecta la placa Arduino al ordenador mediante un cable USB y abre el Arduino IDE. Muestra cómo seleccionar el modelo de placa Arduino y el puerto serial correcto. Se muestran imágenes de la interfaz del Arduino IDE y se resalta la ubicación de las opciones mencionadas.
5. Conclusión: Laura resume los conceptos clave del tutorial y anima a los espectadores a empezar a programar con el Arduino IDE. Menciona que en los próximos videos explorarán más conceptos y aprenderán a utilizar diferentes componentes con el Arduino IDE.

