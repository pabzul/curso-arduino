# Look & feel y estructura

**Escenario:** El video se desarrolla en un entorno de trabajo con una mesa donde se encuentran los componentes necesarios: un Arduino Uno, un LED, una resistencia, una protoboard y los cables de conexión. El presentador está frente a la cámara, listo para demostrar los pasos y explicar los conceptos.

**Estructura del Video:**

1. Introducción y presentación del tema.
2. Lista de componentes necesarios para realizar los ejercicios.
3. Explicación detallada sobre la conexión de la resistencia, el LED, el Arduino y la protoboard.
4. Paso a paso: Conexión de los componentes y explicación de cada conexión.
5. Demostración en vivo: Encendido y apagado del LED.
6. Explicación detallada del código utilizado en la demostración.
7. Paso a paso: Control de la intensidad luminosa del LED.
8. Demostración en vivo: Control de la intensidad luminosa.
9. Explicación de cómo controlar la velocidad de parpadeo de un LED.
10. Demostración en vivo: Control de la velocidad de parpadeo utilizando el ejemplo de LED BLINK.
11. Conclusiones y cierre del video.
