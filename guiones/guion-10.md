---
description: Sensor de Luz
---

# Guión 10

\[Escenario: Un niño sentado en una mesa con una placa Arduino, un protoboard, jumper wires, un sensor de luz, una resistencia de 10K Ohms y un LED con una resistencia de 220 Ohms]&#x20;

(Narrador, Sofía, aparece en pantalla)&#x20;

Sofía: ¡Hola a todos los jóvenes entusiastas de la electrónica! Soy Sofía, y en este emocionante capítulo de nuestros tutoriales de Arduino, vamos a explorar cómo utilizar un sensor de luz para controlar un LED. Descubriremos cómo conectar el sensor de luz LDR a Arduino utilizando un protoboard, jumper wires y las resistencias adecuadas. Además, aprenderemos a realizar una lectura analógica del sensor y a utilizar esos valores para controlar la intensidad de un LED. ¡Así que vamos a sumergirnos en la magia de la luz con Arduino!&#x20;

\[Pantalla muestra el título del capítulo y se enfoca en los componentes en la mesa, incluyendo el protoboard, los jumper wires, el sensor de luz y las resistencias]

Sofía: Antes de comenzar, vamos a echar un vistazo a los componentes que necesitaremos para este proyecto. Primero, necesitaremos una placa Arduino, un protoboard, jumper wires, un sensor de luz LDR, una resistencia de 10K Ohms y un LED con una resistencia de 220 Ohms. ¡Estos son los elementos básicos para explorar el mundo de los sensores de luz!

\[Pantalla muestra una imagen de los componentes necesarios]

Sofía: Comencemos conectando el sensor de luz LDR al protoboard. El sensor de luz LDR tiene dos pines, pero necesitamos agregar una resistencia de 10K Ohms para usarlo correctamente. Conectaremos uno de los extremos de la resistencia a la misma fila en la que conectaremos el pin del sensor de luz LDR. Luego, conectaremos el otro extremo de la resistencia a una fila negativa del protoboard.

\[Pantalla muestra una imagen de las conexiones entre el sensor de luz LDR, la resistencia de 10K Ohms y el protoboard utilizando jumper wires] Sofía: Ahora, vamos a conectar el otro pin del sensor de luz LDR a una fila en la que posteriormente conectaremos un jumper wire. Esta conexión nos permitirá leer los valores de luz utilizando Arduino.

\[Pantalla muestra una imagen de la conexión del segundo pin del sensor de luz LDR al protoboard utilizando un jumper wire]

Sofía: A continuación, conectaremos el LED al protoboard utilizando una resistencia de 220 Ohms para limitar la corriente. Conectaremos el ánodo del LED al lado positivo del protoboard y el cátodo a una fila en la que posteriormente conectaremos un jumper wire.

\[Pantalla muestra una imagen de la conexión del LED al protoboard utilizando una resistencia de 220 Ohms]

Sofía: Ahora, conectaremos un jumper wire desde la fila en la que conectamos el cátodo del LED hacia un pin digital de la placa Arduino, por ejemplo, el pin 9. Esta conexión nos permitirá controlar la intensidad del LED según los valores de luz que leamos.

\[Pantalla muestra una imagen de la conexión del jumper wire desde el protoboard hacia el pin 9 de Arduino]

Sofía: Ahora que hemos realizado todas las conexiones, vamos a escribir el código para controlar el LED según la intensidad de luz que leamos. En el código, utilizaremos la función "analogRead()" para obtener el valor de luz del sensor LDR, y luego utilizaremos la función "analogWrite()" para controlar la intensidad del LED. Cuanto mayor sea el valor de luz, más brillante será el LED.&#x20;

\[Pantalla muestra un fragmento de código con las funciones "analogRead()" y "analogWrite()"]&#x20;

Sofía: Pero, ¿de dónde vienen los valores de 1023 y 255 en el código? Bueno, el sensor LDR produce valores analógicos entre 0 y 1023, donde 0 representa la oscuridad total y 1023 representa la máxima intensidad de luz que puede medir el sensor. Por otro lado, el LED se puede controlar utilizando valores PWM (modulación por ancho de pulso) en Arduino, y el rango para la función "analogWrite()" es de 0 a 255, donde 0 significa apagado y 255 significa brillo máximo. Por eso utilizamos esos valores en nuestro código para mapear los valores de luz al brillo del LED.&#x20;

\[Pantalla muestra una explicación gráfica de los valores de 0 a 1023 y de 0 a 255]

Sofía: Recuerda que también puedes ajustar los límites de lectura y los valores de intensidad del LED según tus preferencias. ¡Experimenta y encuentra la configuración que más te guste!

\[Pantalla muestra una imagen del código completo en el Arduino IDE]

Sofía: Ahora que hemos conectado el sensor de luz, configurado las conexiones y escrito el código, ¡es hora de cargarlo en la placa Arduino y verlo en acción! Una vez cargado, podrás ver cómo el LED se ilumina con mayor intensidad a medida que aumenta la luz ambiental, y se atenúa cuando la luz disminuye.

\[Pantalla muestra imágenes del LED iluminándose con diferentes intensidades según la luz ambiente]

Sofía: ¡Es asombroso cómo podemos utilizar los sensores de luz para interactuar con nuestro entorno! Además de este proyecto, existen muchas otras aplicaciones prácticas para los sensores de luz. Podríamos utilizarlos para crear sistemas de iluminación automática, como luces que se enciendan cuando oscurezca, o para monitorear la luz solar y ajustar la apertura de cortinas o persianas para maximizar el ahorro de energía.

\[Pantalla muestra imágenes de diferentes aplicaciones prácticas de los sensores de luz]

Sofía: Los sensores de luz nos permiten ver y reaccionar ante el mundo invisible de la luz. Con Arduino, podemos crear proyectos emocionantes y explorar infinitas posibilidades. Así que animo a todos a experimentar y descubrir el fascinante mundo de los sensores de luz.

\[Pantalla muestra imágenes de diferentes proyectos caseros utilizando sensores de luz]

Sofía: En nuestro próximo capítulo, seguiremos explorando los sensores y aprenderemos sobre otro tipo emocionante: el sensor de temperatura. ¡No te lo pierdas!

\[Pantalla muestra un avance del próximo capítulo y música de cierre]

Sofía: Eso es todo por hoy, espero que hayan disfrutado de este emocionante proyecto con sensores de luz. Recuerda, la luz es mágica y ahora puedes controlarla con Arduino. ¡Hasta la próxima!

\[Outro musical animada]
