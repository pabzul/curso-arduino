---
description: 'Arduino IDE: Descarga e instalación'
---

# Guión 2

\[Intro musical animado]

\[Créditos de introducción]

(Pablo, sonriente y emocionado, se dirige a la cámara)

Pablo: ¡Hola a todos los amantes de la tecnología! Soy Pablo, su amigo entusiasta de POWAR STEAM, y hoy vamos a continuar nuestra emocionante aventura en el mundo de Arduino. En este capítulo, aprenderemos cómo descargar e instalar el software Arduino IDE y realizar la configuración inicial. ¡Así que prepárense para comenzar a programar!

\[Pablo señala una placa Arduino conectada a un ordenador]

Pablo: Como recordarán, en el capítulo anterior, descubrimos qué es Arduino y por qué es tan genial. Ahora es el momento de obtener el software necesario para empezar a programar nuestras increíbles ideas. ¡Vamos a ello!

\[Pablo muestra la página web de Arduino en la pantalla]

Pablo: Pero antes de empezar, quiero recordarles que todos estos video tutoriales tienen una página en internet que los complementa. En esa página, encontrarán los códigos y esquemas de conexión que aparecen en los video tutoriales. Así que asegúrense de visitar nuestra página en [www.arduino.powarsteam.com](http://www.arduino.powarsteam.com/) para obtener todos los recursos adicionales.

\[Pablo hace un gesto animado de "ven" con su mano]

Pablo: Bien, ahora vamos a descargar el software Arduino IDE. El IDE, o entorno de desarrollo integrado, es como una herramienta especial que nos permite escribir y cargar programas en nuestra placa Arduino. Es como tener un taller tecnológico en nuestro ordenador. ¡Empecemos!

\[Pablo muestra los pasos numerados en la pantalla]

Pablo: Paso número uno: abran su navegador web y visiten el sitio web oficial de Arduino. Aquí está la dirección en pantalla. En la página web, encontrarán un enlace para descargar el Arduino IDE. Hagan clic en ese enlace y esperen a que se descargue el archivo de instalación.

\[Pablo muestra el navegador web con el sitio web de Arduino y un cursor haciendo clic en el enlace de descarga]

Pablo: Paso número dos: una vez que el archivo esté descargado, ábranlo haciendo doble clic en él. Aparecerá una ventana de instalación donde deben seguir los pasos para completar la instalación del Arduino IDE en su ordenador. Es importante leer y aceptar los términos de uso durante el proceso de instalación.

\[Pablo muestra una ventana de instalación y resalta los botones "Siguiente" y "Aceptar"]

Pablo: Paso número tres: una vez instalado el Arduino IDE, necesitamos hacer una configuración inicial para que reconozca la placa Arduino que vamos a utilizar. Pero antes de eso, vamos a conectar nuestra placa Arduino al ordenador utilizando un cable USB.

\[Pablo muestra la placa Arduino conectada al ordenador mediante un cable USB]

Pablo: Ahora, abran el Arduino IDE. Se les mostrará una ventana con una interfaz especial para programar nuestra placa Arduino. Pero antes de empezar a escribir código, debemos asegurarnos de que el IDE esté configurado correctamente para nuestra placa.

\[Pablo muestra la interfaz del Arduino IDE y se resalta la opción de seleccionar el modelo de placa]

Pablo: Vamos a seleccionar el modelo de placa Arduino que estamos utilizando. En la parte superior del IDE, verán un menú desplegable que dice "Herramientas". Hagan clic en ese menú y seleccionen el modelo de su placa Arduino de la lista.

\[Pablo muestra el menú "Herramientas" y una lista de modelos de placas Arduino]

Pablo: Además, debemos seleccionar el puerto serial correcto para nuestra placa Arduino. Esto es importante para establecer la comunicación entre el ordenador y la placa. En el mismo menú desplegable "Herramientas", busquen la opción "Puerto" y elijan el puerto serial correspondiente a su placa.

\[Pablo muestra el menú "Herramientas" con la opción "Puerto" y una lista de puertos disponibles]

Pablo: ¡Y eso es todo! Hemos completado la configuración inicial del entorno de desarrollo. Ahora estamos listos para empezar a programar nuestras ideas y hacerlas realidad con Arduino. Recuerden, si desean obtener todos los componentes necesarios, les invito a visitar nuestro Kit de Introducción a la Robótica de POWAR STEAM en nuestra página web. Ahí encontrarán todo lo que necesitan para seguir este curso completo de Arduino.

\[Pablo muestra el resumen de los pasos de configuración y se enfoca en la placa Arduino y el ordenador]

Pablo: Eso es todo por hoy, amigos y amigas. Espero que les haya sido útil este tutorial sobre la descarga e instalación del Arduino IDE. Recuerden, ¡la programación es divertida y nos permite crear cosas increíbles! No olviden suscribirse a nuestro canal para no perderse ninguno de nuestros emocionantes capítulos.

\[Pablo hace un gesto de "hacia adelante" con su mano]

Pablo: Antes de despedirnos, quiero invitarlos a comprar el Kit de POWAR STEAM en nuestra página de internet, donde podrán encontrar todos los componentes necesarios para este curso de Arduino. Además, si desean profundizar sus conocimientos, les recomendamos visitar nuestro curso completo en [www.arduino.powarsteam.com](http://www.arduino.powarsteam.com/). Allí encontrarán más tutoriales, proyectos y recursos para seguir aprendiendo.

\[Pablo hace un gesto animado de despedida]

Pablo: ¡Gracias por acompañarnos en esta increíble aventura en el mundo de Arduino! ¡Nos vemos en el próximo video!

\[Outro musical animado]
