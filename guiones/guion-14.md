# Guión 14

\[Pantalla muestra el título del tutorial y se enfoca en el sensor ultrasónico de distancia]

Presentador: ¡Hola a todos y bienvenidos a POWAR STEAM! En este tutorial, exploraremos el fascinante mundo de los sensores ultrasónicos de distancia y su uso con Arduino. Aprenderemos a medir distancias y controlar un buzzer en base a esas mediciones. ¡Prepárense para sumergirse en este emocionante proyecto!

\[Pantalla dividida: Presentador en un lado, diagrama del sensor ultrasónico de distancia en el otro]

Presentador: Antes de comenzar, hablemos un poco sobre los sensores ultrasónicos de distancia. Utilizan ondas ultrasónicas para medir la distancia entre el sensor y un objeto cercano, y son ampliamente utilizados en proyectos de robótica, domótica y muchas otras aplicaciones. ¡Sumérgete en este emocionante proyecto!

\[Pantalla muestra los componentes necesarios]

Presentador: Para este proyecto, necesitaremos los siguientes componentes:

* Arduino Uno
* Sensor ultrasónico de distancia HC-SR04
* Buzzer pasivo
* Resistencia de 220 ohmios
* Protoboard
* Jumper wires

\[Presentador sostiene los componentes en sus manos mientras los menciona]

Presentador: Asegúrate de tener todos estos componentes a mano para seguir el tutorial sin problemas.

\[Pantalla muestra la conexión del sensor ultrasónico]

Presentador: Ahora, conectaremos el sensor ultrasónico a nuestro Arduino. Utilizaremos una protoboard y jumper wires para realizar las conexiones.

\[Pantalla dividida: Presentador en un lado, diagrama de conexión en el otro]

Presentador: Conecta el pin VCC del sensor ultrasónico al pin 5V de Arduino, el pin GND al pin GND, el pin TRIGGER al pin digital 2 y el pin ECHO al pin digital 3. Además, conectaremos el buzzer pasivo al pin digital 4 y utilizaremos una resistencia de 220 ohmios en serie para limitar la corriente.

\[Presentador realiza las conexiones en la protoboard mientras explica]

Presentador: Sigue el diagrama de conexión para asegurarte de que todo esté correctamente conectado.

\[Pantalla muestra la instalación de bibliotecas necesarias]

Presentador: Antes de comenzar la programación, necesitamos instalar las bibliotecas necesarias para el sensor ultrasónico. Estas bibliotecas nos proporcionarán funciones y métodos predefinidos que facilitarán la lectura de los valores de distancia.

\[Pantalla dividida: Presentador en un lado, pantalla de la computadora con la página de descarga de la biblioteca en el otro]

Presentador: Dirígete a la página de descarga de la biblioteca en el enlace que se encuentra en la descripción de este video. Descarga la biblioteca y abre el IDE de Arduino.

\[Presentador muestra la página de descarga de la biblioteca y luego abre el IDE de Arduino]

Presentador: Una vez que hayas abierto el IDE de Arduino, ve a "Sketch" -> "Include Library" -> "Add .ZIP Library". Selecciona el archivo de la biblioteca que acabas de descargar y haz clic en "Aceptar". ¡Listo! La biblioteca ahora está instalada y lista para usar.

\[Pantalla muestra la programación del sensor ultrasónico]

Presentador: Ahora, programemos nuestro Arduino para leer los valores de distancia del sensor ultrasónico y mostrarlos en el monitor serial.

\[Pantalla dividida: Presentador en un lado, pantalla de la computadora con el código en el IDE de Arduino en el otro]

Presentador: Abre el IDE de Arduino y comencemos a escribir el código.

\[Cambia a la pantalla de la computadora y muestra el código en el IDE de Arduino]

Presentador: Primero, incluiremos la biblioteca necesaria en la parte superior del código. Luego, declararemos los pines a los que están conectados el TRIGGER y el ECHO. En el setup, configuraremos los pines como INPUT y OUTPUT. Y finalmente, en el loop, realizaremos la lectura de la distancia utilizando los métodos proporcionados por la biblioteca y mostraremos los resultados en el monitor serial.

\[Presentador explica cada parte del código mientras lo escribe]

Presentador: Recuerda comentar el código adecuadamente para facilitar su comprensión.

\[Pantalla muestra el control del buzzer basado en la distancia]

Presentador: Ahora que podemos leer los valores de distancia, vamos a utilizarlos para controlar un buzzer. El buzzer emitirá diferentes tonos según la distancia medida.

\[Pantalla dividida: Presentador en un lado, pantalla de la computadora con el código en el IDE de Arduino en el otro]

Presentador: Modifiquemos nuestro código para agregar la funcionalidad del buzzer.

\[Cambia a la pantalla de la computadora y muestra el código modificado en el IDE de Arduino]

Presentador: Primero, declaramos el pin al que está conectado el buzzer y algunas variables adicionales. Luego, en el loop, utilizamos una estructura condicional para controlar el tono del buzzer según la distancia medida. Si la distancia es menor que 10 cm, emitirá un tono de 1000 Hz. Si está entre 10 y 20 cm, emitirá un tono de 500 Hz. De lo contrario, emitirá un tono de 200 Hz.

\[Presentador explica cada parte del código mientras lo escribe]

Presentador: Carga el código en Arduino y observa cómo el tono del buzzer cambia a medida que mueves objetos cerca y lejos del sensor ultrasónico.

\[Pantalla muestra el resultado final del proyecto]

Presentador: ¡Y eso es todo para este proyecto! Ahora puedes medir distancias y controlar un buzzer utilizando un sensor ultrasónico de distancia con Arduino. Espero que hayas disfrutado de este tutorial y que te haya resultado útil para tus propios proyectos.

\[Pantalla dividida: Presentador en un lado, resultado final del proyecto en el otro]

Presentador: Recuerda experimentar con diferentes distancias y ajustar los tonos del buzzer según tus necesidades. ¡Diviértete explorando las posibilidades!

Presentador: Si te ha gustado este video, no olvides darle like, suscribirte a nuestro canal y activar la campanita para recibir notificaciones cuando subamos nuevos videos. ¡Nos vemos en el próximo proyecto emocionante de POWAR STEAM!

\[Despedida y música de fondo]

Nota: Durante el video, asegúrate de mostrar en pantalla los componentes utilizados, el diagrama de conexión, la página de descarga de la biblioteca y el código en el IDE de Arduino mientras el presentador explica cada paso. También puedes mostrar ejemplos visuales de cómo el sensor ultrasónico detecta la distancia y cómo el buzzer emite diferentes tonos.
