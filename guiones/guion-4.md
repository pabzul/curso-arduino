---
description: Voltaje, Corriente y Resistencia
---

# Guión 4

GUION 6 - C; Corriente Alterna y Continua; Identificación de Resistencias y sus Valores: \[Intro musical animada] \[Créditos de introducción]

(Presentador/a, entusiasta y enérgico/a, se dirige a la cámara)

Presentador/a: ¡Hola a todos y bienvenidos a nuestro canal de POWAR STEAM! En este video tutorial, vamos a adentrarnos en los conceptos básicos de la electricidad y explorar temas como el voltaje, la corriente, la resistencia, la corriente alterna y continua, la identificación de resistencias y sus valores, la ley de Ohm y el cálculo de resistencias en circuitos. Así que prepárate para expandir tus conocimientos en el emocionante mundo de la electricidad. ¡Comencemos!

\[Pantalla muestra el título del capítulo y se enfoca en un diagrama básico de circuito eléctrico]

Presentador/a: Empecemos por comprender los tres conceptos fundamentales de la electricidad: el voltaje, la corriente y la resistencia. Estos conceptos son esenciales para entender cómo funciona la electricidad en los circuitos.

\[Pantalla muestra ejemplos visuales de voltaje, corriente y resistencia]

Presentador/a: El voltaje es la fuerza impulsora que impulsa el flujo de corriente eléctrica en un circuito. Es similar a la presión en una tubería de agua. Se mide en voltios (V) y representa la diferencia de potencial eléctrico entre dos puntos de un circuito.

Presentador/a: La corriente, por otro lado, es el flujo de carga eléctrica a través de un conductor. Podemos imaginarla como el flujo de agua en una tubería. La corriente se mide en amperios (A) y representa la cantidad de carga que fluye por unidad de tiempo.

Presentador/a: Y finalmente, la resistencia es la oposición al flujo de corriente. Imagina que es como un estrechamiento en la tubería que dificulta el paso del agua. La resistencia se mide en ohmios (Ω) y limita la cantidad de corriente que puede fluir en un circuito.

\[Pantalla muestra una analogía visual de una tubería con agua fluyendo y un estrechamiento que representa la resistencia]

Presentador/a: Ahora, hablemos de la diferencia entre la corriente alterna (AC) y la corriente continua (DC). Estos términos se refieren a la forma en que fluye la corriente eléctrica en un circuito.

\[Pantalla muestra ejemplos de corriente alterna (AC) y corriente continua (DC)]

Presentador/a: La corriente alterna es aquella en la que la dirección de flujo de la corriente cambia constantemente en un patrón periódico. Es la corriente que utilizamos en nuestros hogares para la iluminación y los electrodomésticos. Se genera en centrales eléctricas y se distribuye a través de redes eléctricas.

Presentador/a: Por otro lado, la corriente continua es aquella en la que el flujo de corriente es constante en una dirección. La corriente continua proviene de baterías y fuentes de alimentación. Es ampliamente utilizada en dispositivos electrónicos portátiles y circuitos electrónicos que requieren un flujo de corriente constante y unidireccional.

\[Pantalla muestra ejemplos de dispositivos que utilizan corriente alterna y corriente continua]

Presentador/a: Ahora, pasemos a uno de los componentes clave en cualquier circuito: las resistencias. Estos pequeños dispositivos limitan el flujo de corriente y nos permiten controlar la cantidad de corriente que fluye.

\[Pantalla muestra imágenes de diferentes resistencias]

Presentador/a: Identificar una resistencia es crucial para trabajar con ella correctamente. En las resistencias, encontramos bandas de colores que representan su valor. Cada color tiene un valor numérico asignado y una tolerancia. Aprender a leer y comprender estos códigos de colores es esencial.

\[Pantalla muestra ejemplos de bandas de colores en resistencias y sus valores correspondientes]

Presentador/a: Existen herramientas en línea y aplicaciones móviles que facilitan la interpretación de las bandas de colores y proporcionan información precisa sobre el valor de resistencia. Asegúrate de tener una guía de colores a mano para identificar y utilizar resistencias de manera adecuada en tus proyectos electrónicos.

\[Pantalla muestra ejemplos de herramientas en línea y aplicaciones móviles para la identificación de resistencias]

Presentador/a: En este video tutorial, hemos explorado los conceptos básicos de la electricidad, incluyendo el voltaje, la corriente y la resistencia. También hemos aprendido sobre la diferencia entre la corriente alterna y la corriente continua, y cómo identificar resistencias y comprender sus valores.

\[Pantalla muestra resumen visual de los conceptos básicos de electricidad]

Presentador/a: En el próximo video, continuaremos nuestro viaje en el mundo de la electricidad, donde exploraremos la ley de Ohm y aprenderemos a calcular resistencias en circuitos. Así que asegúrate de suscribirte a nuestro canal y activar las notificaciones para no perderte ninguna de nuestras emocionantes lecciones.

\[Pantalla muestra llamado a la acción para suscribirse al canal y activar las notificaciones]

Presentador/a: Gracias por unirte a nosotros hoy. ¡Recuerda que la electricidad es fascinante y está en todas partes! ¡Nos vemos en el próximo video!

\[Outro musical animada] \[Créditos finales]
