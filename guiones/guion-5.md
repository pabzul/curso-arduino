---
description: Ley de Ohms y cálculo de resistencias
---

# Guión 5

\[Corrección del guión para enfocarse únicamente en la ley de Ohm y el cálculo de resistencia]

\[Intro musical animada] \[Créditos de introducción]

(Pablo, sonriente y entusiasta, se dirige a la cámara)

Pablo: ¡Hola a todos los entusiastas de la electrónica! Soy Pablo, y les doy la bienvenida nuevamente a POWAR STEAM, el canal donde descubrirán los fundamentos de la electrónica básica y Arduino. Hoy continuaremos nuestro emocionante viaje en el mundo de la electricidad, ¡así que prepárense para seguir aprendiendo!

\[Pantalla muestra el título del capítulo y se enfoca en un diagrama básico de circuito eléctrico]

Pablo: En esta segunda parte de nuestro capítulo sobre los conceptos básicos de electricidad, nos enfocaremos en la ley de Ohm y el cálculo de resistencias. Estos conceptos son fundamentales para comprender cómo funciona la corriente eléctrica en un circuito y cómo calcular los valores de resistencia.

\[Pantalla muestra la fórmula de la ley de Ohm]

Pablo: Comencemos con la ley de Ohm. Esta ley establece la relación entre el voltaje (V), la corriente (I) y la resistencia (R) en un circuito. La fórmula básica es V = I \* R, donde V es el voltaje, I es la corriente y R es la resistencia.

\[Pantalla muestra ejemplos de aplicación de la ley de Ohm en diferentes circuitos]

Pablo: Podemos utilizar esta fórmula para calcular cualquiera de estos valores si conocemos los otros dos. Por ejemplo, si tenemos un circuito con un voltaje de 12 voltios y una resistencia de 4 ohmios, podemos calcular la corriente utilizando la ley de Ohm. Simplemente dividimos el voltaje entre la resistencia, lo que nos dará una corriente de 3 amperios.

\[Pantalla muestra ejemplos de cálculo de resistencias en diferentes circuitos]

Pablo: Es importante comprender cómo calcular resistencias en circuitos más complejos. En un circuito en serie, las resistencias se suman, mientras que en un circuito en paralelo, se aplica una fórmula especial. Estos conceptos nos permiten diseñar y analizar circuitos eléctricos de manera eficiente.

\[Pantalla muestra ejemplos de circuitos en serie y en paralelo]

Pablo: En futuros capítulos, profundizaremos en estos conceptos y los pondremos en práctica con ejemplos y proyectos interesantes. Así que asegúrate de suscribirte a nuestro canal y activar las notificaciones para no perderte ninguna de nuestras emocionantes lecciones.

\[Pantalla muestra llamado a la acción para suscribirse al canal y activar las notificaciones]

Pablo: Y eso es todo por hoy, amigos. Espero que hayan disfrutado de esta segunda parte del capítulo sobre los conceptos básicos de electricidad. Recuerden que en el Kit de Introducción a la Robótica de POWAR STEAM pueden conseguir todos los componentes necesarios para seguir este curso de Arduino. ¡Nos vemos en el próximo video para seguir aprendiendo sobre electrónica! ¡Hasta pronto!

\[Outro musical animada]
