# Guión 13

\[Intro musical animada] \[Créditos de introducción]

(Pablo, sonriente y entusiasta, se dirige a la cámara) Pablo: ¡Hola a todos los entusiastas de la electrónica! Soy Pablo, y les doy la bienvenida nuevamente a POWAR STEAM, el canal donde descubrirán los fundamentos de la electrónica básica y Arduino. En este tutorial, aprenderemos a utilizar el sensor de temperatura y humedad DHT11 con Arduino y a controlar un motor DC con una hélice de ventilador. ¡Comencemos!

\[Pantalla muestra el título del tutorial y se enfoca en el sensor DHT11]

Pablo: El sensor DHT11 nos permite medir la temperatura y la humedad relativa del aire. Aprenderemos a realizar la conexión, instalar las bibliotecas necesarias, leer y visualizar los valores de temperatura y humedad, y controlar un motor DC en función de la temperatura medida.

\[Pantalla muestra la conexión del sensor DHT11 a Arduino]

Pablo: Para comenzar, conectaremos el sensor DHT11 a nuestro Arduino de la siguiente manera:

* Pin VCC del sensor al pin 5V de Arduino.
* Pin DATA del sensor al pin digital 2 de Arduino.
* Pin GND del sensor al pin GND de Arduino.

\[Pantalla muestra la instalación de bibliotecas necesarias]

Pablo: Antes de programar, necesitaremos instalar las bibliotecas adecuadas para el sensor DHT11. En el IDE de Arduino, abriremos el Administrador de Bibliotecas, buscaremos "DHT" y seleccionaremos la biblioteca "DHT sensor library" de Adafruit.

\[Pantalla muestra la lectura y visualización de valores de temperatura y humedad]

Pablo: Con la conexión y las bibliotecas instaladas, pasaremos a la programación. Importaremos la biblioteca DHT, definiremos los pines de conexión y crearemos una instancia del sensor. Utilizaremos las funciones "readTemperature" y "readHumidity" para obtener los valores de temperatura y humedad respectivamente, y los mostraremos en el monitor serial utilizando la función "Serial.print".

\[Pantalla muestra la conexión del motor DC y la programación para controlarlo]

Pablo: Además, aprenderemos a utilizar los valores de temperatura para controlar un motor DC con una hélice de ventilador. Si la temperatura supera un umbral predefinido, activaremos el motor utilizando la función "analogWrite" para controlar la velocidad. De esta manera, crearemos un flujo de aire refrescante cuando sea necesario.

\[Conclusión]

Pablo: ¡Excelente trabajo! Ahora sabemos cómo utilizar el sensor de temperatura y humedad DHT11 con Arduino, así como controlar un motor DC en función de la temperatura medida. Recuerden experimentar y explorar nuevas ideas en el apasionante mundo de Arduino. Para más tutoriales y proyectos emocionantes, visiten nuestro blog POWAR STEAM. ¡Hasta la próxima!

\[Outro musical animada]
