# Guión 12

\[Intro Music]

Host: ¡Hola a todos, entusiastas de POWAR STEAM! Bienvenidos a nuestro canal de YouTube. En el día de hoy, vamos a adentrarnos en el fascinante mundo de los motores DC y aprenderemos a utilizarlos como actuadores en nuestros proyectos con Arduino. Así que prepárense para dominar el Motor DC y realizar proyectos increíbles.

\[Transition]

Host: Antes de comenzar, déjenme explicarles qué es un motor DC. Un motor DC, o corriente continua, es un dispositivo electromecánico que convierte la energía eléctrica en movimiento mecánico. Estos motores vienen en diferentes tamaños y potencias, y funcionan mediante la interacción de imanes y corriente eléctrica para impulsar el movimiento del eje del motor.

\[Transition]

Host: Pero antes de adentrarnos en los proyectos prácticos, es importante mencionar otros tipos de motores comunes en proyectos de electrónica y robótica. Uno de ellos son los servos, que son motores controlados por posición y son ideales para movimientos precisos, como brazos robóticos o control de dirección en vehículos. Otro tipo son los motores paso a paso, que se mueven en incrementos discretos y predefinidos, perfectos para proyectos que requieren movimientos precisos y repetitivos, como impresoras 3D o máquinas CNC.

\[Transition]

Host: Ahora que conocemos un poco más sobre los motores DC y otros tipos de motores, es hora de aprender cómo podemos controlar los motores utilizando diferentes componentes. Vamos a comenzar con el control de velocidad del motor DC mediante código en Arduino.

\[Transition]

Host: En nuestro primer proyecto, aprenderemos a controlar la velocidad del motor DC utilizando el código en Arduino. Para este proyecto, necesitarán los siguientes materiales: un Arduino Uno, un motor DC, una protoboard y cables jumper.

\[Transition]

Host: Comencemos realizando las conexiones. Conecten el pin positivo (+) del motor DC al pin digital 9 del Arduino utilizando un cable jumper. Luego, conecten el pin negativo (-) del motor DC al pin GND (tierra) del Arduino utilizando otro cable jumper.

\[Transition]

Host: Ahora, abran el IDE de Arduino y utilicen el siguiente código para controlar la velocidad del motor DC:

\[On-Screen Code Display]

Host: Este código utiliza la función "analogWrite()" para establecer la velocidad del motor DC en valores entre 0 y 255. Pueden experimentar con diferentes valores para ajustar la velocidad según sus preferencias. Hemos agregado un segundo cambio de velocidad para mostrar cómo combinar diferentes valores y tiempos de espera para lograr efectos interesantes.

\[Transition]

Host: ¡Y ahí lo tienen! Ahora saben cómo controlar la velocidad de un motor DC utilizando código en Arduino. Pero eso no es todo, ¡hay muchas más formas creativas de controlar los motores DC!

\[Transition]

Host: Uno de los componentes más populares para controlar motores DC es el potenciómetro. En nuestro segundo proyecto, vamos a utilizar un potenciómetro para controlar la velocidad del motor DC.

\[Transition]

Host: Para este proyecto, necesitarán los siguientes materiales: un Arduino Uno, un motor DC, un potenciómetro, una protoboard y cables jumper.

\[Transition]

Host: Realicemos las conexiones. Conecten uno de los pines del potenciómetro al pin analógico A0 del Arduino. Luego, conecten el pin central (wiper) del potenciómetro a una línea de alimentación de 5V en la protoboard. Finalmente, conecten el pin positivo (+) del motor DC al pin digital 9 del Arduino, y el pin negativo (-) del motor DC al pin GND (tierra) del Arduino.

\[Transition]

Host: Ahora, utilizaremos el siguiente código para leer los valores del potenciómetro y controlar la velocidad del motor en función de esos valores.

\[On-Screen Code Display]

Host: Este código lee los valores analógicos del potenciómetro utilizando la función "analogRead()" y luego utiliza la función "map()" para ajustar esos valores al rango de velocidad del motor (de 0 a 255). Finalmente, establece la velocidad del motor utilizando la función "analogWrite()" con el valor mapeado.

\[Transition]

Host: ¡Y eso es todo! Ahora saben cómo controlar la velocidad de un motor DC utilizando un potenciómetro. Recuerden que además de los potenciómetros, existen muchos otros componentes y dispositivos que se pueden utilizar para controlar los motores DC, como sensores, botones, controladores de motor, joysticks y más. La elección del componente dependerá de los requisitos específicos de su proyecto y de las funcionalidades que deseen implementar.

\[Transition]

Host: En resumen, en este video hemos explorado el emocionante mundo de los motores DC, aprendiendo cómo controlar su velocidad utilizando Arduino. Hemos comprendido qué es un motor DC, sus diferentes tamaños y potencias, y hemos mencionado otros tipos de motores como servos y motores paso a paso. Además, hemos descubierto que los motores pueden ser controlados utilizando una variedad de componentes. Ahora les animamos a que experimenten con estos proyectos y exploren formas creativas de utilizar los motores DC en sus propios proyectos.

\[Outro]

Host: ¡Gracias por acompañarnos en este video tutorial! Si les ha gustado, no olviden darle like y suscribirse a nuestro canal para más contenido sobre electrónica y programación. Si tienen alguna pregunta, déjenla en los comentarios y estaremos encantados de ayudarles. ¡Nos vemos en el próximo video!

\[End Music]
