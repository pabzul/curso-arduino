---
description: Control de LEDs con Resistencias
---

# Guión 7

**Presentador:** ¡Hola a todos y bienvenidos nuevamente a nuestro canal de tutoriales de Arduino! En este capítulo, vamos a adentrarnos en el emocionante mundo de los LEDs y aprender cómo controlarlos utilizando resistencias. Pero primero, veamos la lista de componentes que necesitaremos para realizar los ejercicios.

**\[Lista de componentes necesarios: Arduino Uno, LED, resistencia, protoboard, cables de conexión]**

**Presentador:** Ahora que tenemos todos los componentes necesarios, vamos a pasar a la conexión de los mismos. Es importante seguir los pasos correctos para asegurarnos de que todo funcione correctamente. Presten atención a la explicación detallada de cada conexión.

**\[Imágenes paso a paso que muestran cómo conectar la resistencia, el LED, el Arduino y la protoboard]**

**Presentador:** Muy bien, ahora que hemos conectado correctamente todos los componentes, vamos a encender y apagar el LED. Esto nos permitirá comprobar que la conexión está funcionando correctamente. Presten atención a la demostración en vivo.

**\[Demostración en vivo del encendido y apagado del LED utilizando el código y la función digitalWrite()]**

**Presentador:** ¡Genial! Ahora que hemos encendido y apagado el LED, es hora de explorar cómo controlar la intensidad luminosa. Esto nos permitirá ajustar la luminosidad del LED según nuestras preferencias. Presten atención a la explicación detallada del código que utilizaremos.

**\[Explicación detallada del código utilizado para controlar la intensidad luminosa utilizando la función analogWrite()]**

**Presentador:** Ahora que hemos comprendido cómo controlar la intensidad luminosa, es hora de ponerlo en práctica. Presten atención a la demostración en vivo donde ajustaremos la intensidad luminosa del LED.

**\[Demostración en vivo del control de la intensidad luminosa del LED utilizando el código y la función analogWrite()]**

**Presentador:** ¡Fantástico! Ahora, quiero compartirles un truco adicional: cómo controlar la velocidad de parpadeo de un LED. Para hacer esto, utilizaremos el ejemplo de LED BLINK que viene incluido en el Arduino IDE. Presten atención a la explicación detallada.

**\[Explicación detallada de cómo controlar la velocidad de parpadeo utilizando el ejemplo de LED BLINK]**

**Presentador:** ¡Y ahora, veamos en acción el control de la velocidad de parpadeo utilizando el ejemplo de LED BLINK! Presten atención a la demostración en vivo.

**\[Demostración en vivo del control de la velocidad de parpadeo utilizando el ejemplo de LED BLINK]**

**Presentador:** En este capítulo, hemos aprendido cómo controlar LEDs utilizando resistencias. Hemos explorado cómo encender y apagar un LED, controlar su intensidad luminosa y también cómo ajustar la velocidad de parpadeo utilizando el ejemplo de LED BLINK. Estos conocimientos les serán útiles en futuros proyectos y podrán dar rienda suelta a su creatividad.

**Cierre del Video:** En el cierre del video, el presentador agradece a los espectadores por acompañarlos en este tutorial y los invita a dejar comentarios, preguntas y sugerencias. Se despide de manera amigable y motiva a los espectadores a seguir explorando el mundo de la electrónica y la programación con Arduino.

¡Y eso es todo para este capítulo! Espero que les haya gustado el video y que haya sido útil para comprender cómo controlar LEDs con resistencias. Si tienen alguna pregunta, no duden en dejarla en los comentarios. ¡Nos vemos en el próximo tutorial!
