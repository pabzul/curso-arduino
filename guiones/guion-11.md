# Guión 11

\[Escenario: Un niño sentado en una mesa con una placa Arduino y diversos actuadores]

(Narrador, Sofía, aparece en pantalla)

Sofía: ¡Saludos, entusiastas de Arduino! Soy Sofía, y en este emocionante capítulo de nuestros tutoriales en POWAR STEAM, vamos a explorar el increíble poder de los actuadores. Los actuadores son componentes electrónicos que nos permiten dar vida y movimiento a nuestros proyectos.

Hoy descubriremos algunos de los actuadores más comunes y exploraremos algunas ideas emocionantes de proyectos que puedes hacer en casa. ¡Así que pongámonos en marcha y descubramos el mundo del movimiento con Arduino!

\[Pantalla muestra el título del capítulo y se enfoca en los componentes en la mesa, incluyendo los diferentes actuadores]

Sofía: Antes de comenzar, echemos un vistazo a los diferentes tipos de actuadores que existen. Tenemos motores, servos, relés, luces, ventiladores y más. Estos dispositivos nos permiten controlar el movimiento, encendido y apagado de diferentes elementos en nuestros proyectos. ¡Vamos a explorar cómo podemos utilizarlos para crear cosas increíbles!

\[Pantalla muestra una imagen de los diferentes actuadores mencionados]

Sofía: Ahora, hablemos de algunos proyectos emocionantes que puedes realizar con actuadores. Imagina construir tu propio robot con motores y servos, diseñar un sistema de iluminación inteligente con luces LED o desarrollar un sistema de enfriamiento automático para tu computadora con un ventilador controlado por Arduino. ¡Las posibilidades son infinitas!

\[Pantalla muestra imágenes de los proyectos mencionados]

Sofía: Estos son solo ejemplos para inspirarte, pero recuerda que puedes adaptarlos y personalizarlos según tus propias ideas y necesidades. La clave es utilizar tu creatividad y experimentar con los actuadores para dar vida a tus proyectos.

\[Pantalla muestra una imagen de un proyecto en desarrollo con diferentes actuadores]

Sofía: Además de los proyectos caseros, los actuadores tienen aplicaciones prácticas en nuestra vida diaria. Pueden encontrarse en la domótica, la industria automotriz, la robótica y más. Imagina tener un hogar inteligente donde puedas controlar luces, persianas y temperatura con solo presionar un botón, o cómo los actuadores son fundamentales en los vehículos modernos para el control de motores y sistemas de cierre centralizado.

\[Pantalla muestra imágenes de aplicaciones prácticas de los actuadores]

Sofía: ¡Es fascinante cómo los actuadores nos permiten interactuar con el mundo que nos rodea! Con Arduino, podemos explorar el poder del movimiento y la acción, creando proyectos emocionantes y dando vida a nuestras ideas. ¡Así que adelante, experimenta y descubre todo lo que los actuadores pueden hacer!

\[Pantalla muestra imágenes de diferentes proyectos caseros utilizando actuadores]

Sofía: En nuestro próximo capítulo, vamos a sumergirnos en el mundo de los motores DC. Aprenderemos cómo controlar estos motores para generar movimiento en nuestros proyectos. ¡No te lo pierdas!

\[Pantalla muestra un avance del próximo capítulo y música de cierre]

Sofía: Eso es todo por hoy, espero que hayas disfrutado de este emocionante vistazo al poder de los actuadores. Recuerda, con Arduino y POWAR STEAM, puedes dar vida a tus ideas y crear proyectos sorprendentes. ¡Suscríbete a nuestro canal para no perderte el próximo capítulo! ¡Hasta la próxima!

\[Outro musical animado]
